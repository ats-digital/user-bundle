<?php declare(strict_types=1);

namespace ATS\UserBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * PasswordStrength
 *
 * @Annotation
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
class PasswordStrength extends Constraint
{
    /**
     * @var string
     */
    public $message = 'error.validation.password_strength.%s';
}
