<?php declare(strict_types=1);

namespace ATS\UserBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * PasswordStrengthValidator
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
class PasswordStrengthValidator extends ConstraintValidator
{
    const MINIMUM_LENGTH  = 6;
    const WEAK_STRENGTH   = "weak";
    const LOW_STRENGTH    = "low";
    const MEDIUM_STRENGTH = "medium";
    const STRONG_STRENGTH = "strong";

    /**
     * @var string
     */
    private $strength;

    /**
     * Constructor
     *
     * @param string $strength
     */
    public function __construct($strength)
    {
        $this->strength = $strength;
    }

    /**
     * {@inheritDoc}
     */
    public function validate($value, Constraint $constraint)
    {
        if (is_string($value) === false) {
            throw new UnexpectedTypeException($value, 'string');
        }

        $pattern = '';
        switch ($this->strength) {
            case self::WEAK_STRENGTH:
                $pattern = '/^\S*(?=\S{' . self::MINIMUM_LENGTH . ',})\S*$/';
                break;

            case self::LOW_STRENGTH:
                $pattern = '/^\S*(?=\S{' . self::MINIMUM_LENGTH . ',})(?=\S*[A-Z])\S*$/';
                break;

            case self::MEDIUM_STRENGTH:
                $pattern = '/^\S*(?=\S{' . self::MINIMUM_LENGTH . ',})(?=\S*[a-z]?)(?=\S*[A-Z])(?=\S*[\d])(?=\S*[\d])(?=\S*[\&\@\?\!\(){}\/\+\-*~\#_]?)\S*$/';
                break;

            case self::STRONG_STRENGTH:
                $pattern = '/^\S*(?=\S{' . self::MINIMUM_LENGTH . ',})(?=\S*[a-z]?)(?=\S*[A-Z])(?=\S*[\d])(?=\S*[\d])(?=\S*[\&\@\?\!\(){}\/\+\-*~\#_])\S*$/';
                break;

            default:
                $pattern = '/^\S*(?=\S{1,})\S*$/';
                break;
        }

        if (preg_match($pattern, $value, $matches) === 0) {
            $this->context->buildViolation(
                sprintf($constraint->message, $this->strength)
            )->addViolation();
        }
    }
}
