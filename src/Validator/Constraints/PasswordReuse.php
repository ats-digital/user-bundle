<?php declare(strict_types=1);

namespace ATS\UserBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * PasswordReuse
 *
 * @Annotation
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
class PasswordReuse extends Constraint
{
    /**
     * @var string
     */
    public $message = 'error.validation.password_reuse';
}
