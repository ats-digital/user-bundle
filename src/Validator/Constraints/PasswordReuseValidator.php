<?php declare(strict_types=1);

namespace ATS\UserBundle\Validator\Constraints;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface as Encoder;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;

/**
 * PasswordReuseValidator
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
class PasswordReuseValidator extends ConstraintValidator
{
    /**
     * @var Encoder
     */
    protected $encoder;

    /**
     * @var string
     */
    private $userClass;

    /**
     * @var bool
     */
    private $reuse;

    /**
     * Constructor
     *
     * @param Encoder $encoder
     * @param string  $userClass
     * @param bool    $reuse
     */
    public function __construct(Encoder $encoder, $userClass, $reuse)
    {
        $this->encoder = $encoder;
        $this->userClass = $userClass;
        $this->reuse = $reuse;
    }

    /**
     * {@inheritDoc}
     */
    public function validate($value, Constraint $constraint)
    {
        if ($this->reuse === true || count($value) === 0) {
            return;
        }

        if (is_array($value) === false) {
            throw new UnexpectedTypeException($value, 'array');
        }

        $newPasswordIndex = count($value) - 1;
        $newPassword = $value[$newPasswordIndex];
        unset($value[$newPasswordIndex]);
        $newPassword = $this->encoder->encodePassword((new $this->userClass), $newPassword);

        if (in_array($newPassword, $value) === true) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}
