<?php declare (strict_types = 1);

namespace ATS\UserBundle\Repository;

use ATS\CoreBundle\Repository\BaseDocumentRepository;

/**
 * UserRepository
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
class UserRepository extends BaseDocumentRepository
{
    /**
     * Get all users
     *
     * @return array
     */
    public function getAll()
    {
        return $this->createQueryBuilder()->getQuery()->execute()->toArray();
    }

    /**
     * Load user by username or email
     *
     * @param string $criteria
     *
     * @return array|object|null
     */
    public function loadUserByUsernameOrEmail($criteria)
    {
        $qb = $this->createQueryBuilder();
        $qb->addOr(
            $qb->expr()
                ->field('username')->exists(true)
                ->field('username')->equals($criteria)
        );
        $qb->addOr(
            $qb->expr()
                ->field('email')->exists(true)
                ->field('email')->equals($criteria)
        );

        return $qb->getQuery()->getSingleResult();
    }

    /**
     * Load user by username and resetToken
     *
     * @param string $resetToken
     *
     * @return array|object|null
     */
    public function loadUserByResetToken($resetToken)
    {
        $qb = $this->createQueryBuilder()
            ->field('resetToken')->exists(true)
            ->field('resetToken')->equals($resetToken);

        return $qb->getQuery()->getSingleResult();
    }
}
