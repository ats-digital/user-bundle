<?php declare (strict_types = 1);

namespace ATS\UserBundle\Repository;

use ATS\CoreBundle\Repository\BaseDocumentRepository;

/**
 * RefreshTokenRepository
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
class RefreshTokenRepository extends BaseDocumentRepository
{
}
