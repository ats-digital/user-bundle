<?php declare (strict_types = 1);

namespace ATS\UserBundle\Service;

use Symfony\Component\Config\Definition\Exception\MongoDuplicateKeyException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use ATS\EmailBundle\Service\SimpleMailerService;
use Symfony\Component\HttpFoundation\Response;
use ATS\EmailBundle\Manager\EmailManager;
use ATS\UserBundle\Manager\UserManager;
use ATS\EmailBundle\Document\Email;
use JMS\Serializer\Serializer;

/**
 * UserService
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
class UserService
{
    /**
     * @var UserManager
     */
    protected $userManager;

    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * @var Serializer
     */
    protected $serializer;

    /**
     * @var string
     */
    protected $userClass;

    /**
     * @var EngineInterface
     */
    protected $templatingEngine;

    /**
     * @var EmailManager
     */
    protected $emailManager;

    /**
     * @var mixed
     */
    protected $mailer;

    /**
     * @var array
     */
    protected $resetPasswordSettings;

    /**
     * Constructor
     *
     * @param UserManager        $userManager
     * @param ValidatorInterface $validator
     * @param EngineInterface    $templatingEngine
     * @param EmailManager       $emailManager
     */
    public function __construct(UserManager $userManager, ValidatorInterface $validator, EngineInterface $templatingEngine, EmailManager $emailManager)
    {
        $this->userManager = $userManager;
        $this->validator = $validator;
        $this->templatingEngine = $templatingEngine;
        $this->emailManager = $emailManager;
    }

    /**
     * Set serializer
     *
     * @param Serializer $serializer
     */
    public function setSerializer($serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * Set userClass
     *
     * @param string $userClass
     */
    public function setUserClass($userClass)
    {
        $this->userClass = $userClass;
    }

    /**
     * Set mailer
     *
     * @param mixed $mailer
     */
    public function setMailer($mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Set resetPasswordSettings
     *
     * @param string $template
     * @param string $callbackUri
     * @param string $tokenParameter
     * @param string $subject
     * @param string $senderName
     * @param string $senderEmail
     * @param bool   $save
     */
    public function setResetPasswordSettings($template, $callbackUri, $tokenParameter, $subject, $senderName, $senderEmail, $save)
    {
        $this->resetPasswordSettings = [
            'template' => $template,
            'callbackUri' => $callbackUri,
            'tokenParameter' => $tokenParameter,
            'subject' => $subject,
            'senderName' => $senderName,
            'senderEmail' => $senderEmail,
            'save' => $save,
        ];
    }

    /**
     * Create
     *
     * @param string $userData
     *
     * @return array
     */
    public function create($userData)
    {
        return $this->updateUser($userData, ['registration']);
    }

    /**
     * Update
     *
     * @param string $userData
     *
     * @return array
     */
    public function update($userData)
    {
        return $this->updateUser($userData, ['update']);
    }

    /**
     * Update user
     *
     * @param string $userData
     * @param array  $validationGroups
     *
     * @return array
     */
    protected function updateUser(string $userData, array $validationGroups)
    {
        if ($userData === "") {
            $userData = json_encode([]);
        }
        $user = $this->serializer->deserialize($userData, $this->userClass, 'json');
        $errors = $this->validator->validate($user, null, $validationGroups);

        if (count($errors) === 0) {
            try {
                $userId = $this->userManager->update($user);

                return [
                    'statusCode' => Response::HTTP_OK,
                    'content' => [
                        'user' => 'success.action.update',
                        'id' => $userId,
                    ],
                ];
            } catch (\MongoDuplicateKeyException $e) {
                $response = ['statusCode' => Response::HTTP_BAD_REQUEST];
                if (strpos($e->getMessage(), '$_username') !== false) {
                    $response['content'] = ['username' => 'error.validation.exist'];
                }
                if (strpos($e->getMessage(), '$_email') !== false) {
                    $response['content'] = ['email' => 'error.validation.exist'];
                }

                return $response;
            }
        }

        $response = ['statusCode' => Response::HTTP_BAD_REQUEST];
        foreach ($errors as $error) {
            $response['content'][$error->getPropertyPath()] = $error->getMessage();
        }

        return $response;
    }

    /**
     * Reset password by username or email
     *
     * @param string $requestData
     *
     * @return array
     */
    public function resetPassword($requestData)
    {
        $userData = json_decode($requestData, true);
        if (isset($userData['username']) === false) {
            return [
                'statusCode' => Response::HTTP_BAD_REQUEST,
                'content' => [
                    'response' => 'reset_password.missing.username',
                ],
            ];
        }
        $statusCode = Response::HTTP_OK;
        $response = null;
        try {
            /** @var \ATS\UserBundle\Document\User $user */
            $user = $this->userManager->resetPassword($userData['username']);
            $resetPasswordCallbackUri = sprintf(
                '%s?%s=%s',
                $this->resetPasswordSettings['callbackUri'],
                $this->resetPasswordSettings['tokenParameter'],
                $user->getResetToken()
            );
            $parameters = [
                'link' => $resetPasswordCallbackUri,
            ];

            $email = new Email();
            $email->setTemplate($this->resetPasswordSettings['template'])
                  ->setMessageParameters($parameters)
                  ->setSubject($this->resetPasswordSettings['subject'])
                  ->setSenderAddress($this->resetPasswordSettings['senderEmail'])
                  ->setSenderName($this->resetPasswordSettings['senderName'])
                  ->setRecipientAddress((string) $user->getEmail())
                  ->setSentAt(new \DateTime('now'));

            $mailerService = new SimpleMailerService($this->mailer, $this->templatingEngine, $this->emailManager);
            $mailerService->send($email, $this->resetPasswordSettings['save']);
            $response = 'reset_password.success';
        } catch (NotFoundHttpException $e) {
            $statusCode = $e->getStatusCode();
            $response = $e->getMessage();
        }

        return [
            'statusCode' => $statusCode,
            'content' => [
                'response' => $response,
            ],
        ];
    }

    /**
     * change password by username or email and reset token
     *
     * @param string $requestData
     *
     * @return array
     */
    public function changePassword($requestData)
    {
        $userData = json_decode($requestData, true);
        if (isset($userData['reset_token']) === false) {
            return [
                'statusCode' => Response::HTTP_BAD_REQUEST,
                'content' => [
                    'response' => 'reset_password.missing.token',
                ],
            ];
        }
        if (isset($userData['plain_password']) === false) {
            return [
                'statusCode' => Response::HTTP_BAD_REQUEST,
                'content' => [
                    'response' => 'reset_password.missing.plain_password',
                ],
            ];
        }

        $statusCode = Response::HTTP_OK;
        $response = null;
        try {
            $user = $this->userManager->changePasswordByResetToken($userData['reset_token'], $userData['plain_password']);
            $errors = $this->validator->validate($user);
            if (count($errors) > 0) {
                foreach ($errors as $error) {
                    $response[$error->getPropertyPath()] = $error->getMessage();
                }

                return [
                    'statusCode' => Response::HTTP_BAD_REQUEST,
                    'content' => [
                        'response' => $response,
                    ],
                ];
            }
            $this->userManager->update($user);
            $response = 'change_password.success';
        } catch (HttpException $e) {
            $statusCode = $e->getStatusCode();
            $response = $e->getMessage();
        }

        return [
            'statusCode' => $statusCode,
            'content' => [
                'response' => $response,
            ],
        ];
    }
}
