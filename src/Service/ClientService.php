<?php declare (strict_types = 1);

namespace ATS\UserBundle\Service;

use Symfony\Component\Config\Definition\Exception\MongoDuplicateKeyException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use ATS\UserBundle\Manager\ClientManager;
use ATS\UserBundle\Document\Client;

/**
 * ClientService
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
class ClientService
{
    /**
     * @var ClientManager
     */
    protected $clientManager;

    /**
     * @var string
     */
    protected $redirectUri;

    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * Constructor
     *
     * @param ClientManager      $clientManager
     * @param ValidatorInterface $validator
     */
    public function __construct(ClientManager $clientManager, ValidatorInterface $validator)
    {
        $this->clientManager = $clientManager;
        $this->validator = $validator;
    }

    /**
     * Set redirectUri
     *
     * @param string $scheme
     * @param string $host
     */
    public function setRedirectUri($scheme, $host)
    {
        $this->redirectUri = sprintf('%s://%s', $scheme, $host);
    }

    /**
     * Get clients
     *
     * @return array
     */
    public function getClients()
    {
        return $this->clientManager->getAll();
    }

    /**
     * Create client
     *
     * @param string $name
     *
     * @return array
     */
    public function create($name = null)
    {
        /** @var Client */
        $client = $this
            ->clientManager
            ->getBaseClientManager()
            ->createClient();

        $client->setName($name);
        $client->setRedirectUri($this->redirectUri);

        $errors = $this->validator->validate($client);

        if (count($errors) === 0) {
            try {
                $clientId = $this->clientManager->update($client);

                return [
                    'status' => true,
                    'content' => [
                        'client' => 'success.action.create',
                        'id' => $clientId,
                    ],
                ];
            } catch (\MongoDuplicateKeyException $e) {
                $response = ['status' => false];
                if (strpos($e->getMessage(), '$_name') !== false) {
                    $response['content'] = ['name' => 'error.validation.exist'];
                }

                return $response;
            }
        }

        $response = ['status' => false];
        foreach ($errors as $error) {
            $response['content'][$error->getPropertyPath()] = $error->getMessage();
        }

        return $response;
    }
}
