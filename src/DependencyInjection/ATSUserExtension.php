<?php declare(strict_types=1);

namespace ATS\UserBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\Config\FileLocator;

/**
 * UserExtension
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
class ATSUserExtension extends Extension implements PrependExtensionInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter(
            'ats_user.password_strength',
            $config['password_strength']
        );
        $container->setParameter(
            'ats_user.password_reuse',
            $config['password_reuse']
        );
        $container->setParameter(
            'ats_user.user_class',
            $config['user_class']
        );
        $container->setParameter(
            'ats_user.roles_definition',
            $config['roles_definition']
        );
        $container->setParameter(
            'ats_user.ip_whitelist',
            $config['ip_whitelist']
        );
        $container->setParameter(
            'ats_user.ip_blacklist',
            $config['ip_blacklist']
        );
        $container->setParameter(
            'ats_user.routing.host',
            $config['routing']['host']
        );
        $container->setParameter(
            'ats_user.routing.prefix',
            $config['routing']['prefix']
        );
        $container->setParameter(
            'ats_user.routing.scheme',
            $config['routing']['scheme']
        );
        $authTokenPath = $config['routes']['oauth_token'];
        $authTokenPath = substr($authTokenPath, 0, 1) === '/' ? substr($authTokenPath, 1) : $authTokenPath;
        $container->setParameter(
            'ats_user.routes.oauth_token',
            $authTokenPath
        );
        $registerPath = $config['routes']['register'];
        $registerPath = substr($registerPath, 0, 1) === '/' ? substr($registerPath, 1) : $registerPath;
        $container->setParameter(
            'ats_user.routes.register',
            $registerPath
        );
        $resetPasswordPath = $config['routes']['reset_password'];
        $resetPasswordPath = substr($resetPasswordPath, 0, 1) === '/' ? substr($resetPasswordPath, 1) : $resetPasswordPath;
        $container->setParameter(
            'ats_user.routes.reset_password',
            $resetPasswordPath
        );
        $changePasswordPath = $config['routes']['change_password'];
        $changePasswordPath = substr($changePasswordPath, 0, 1) === '/' ? substr($changePasswordPath, 1) : $changePasswordPath;
        $container->setParameter(
            'ats_user.routes.change_password',
            $changePasswordPath
        );
        $container->setParameter(
            'ats_user.template.reset_password',
            $config['template']['reset_password']
        );
        $container->setParameter(
            'ats_user.reset_password.callback_uri',
            $config['reset_password']['callback_uri']
        );
        $container->setParameter(
            'ats_user.reset_password.token_parameter',
            $config['reset_password']['token_parameter']
        );
        $container->setParameter(
            'ats_user.reset_password.subject',
            $config['reset_password']['subject']
        );
        $container->setParameter(
            'ats_user.reset_password.sender_name',
            $config['reset_password']['sender_name']
        );
        $container->setParameter(
            'ats_user.reset_password.sender_email',
            $config['reset_password']['sender_email']
        );
        $container->setParameter(
            'ats_user.reset_password.save',
            $config['reset_password']['save']
        );

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
    }

    /**
     * {@inheritdoc}
     */
    public function prepend(ContainerBuilder $container)
    {
        $bundles = $container->getParameter('kernel.bundles');
        if (isset($bundles['ATSUserBundle']) !== false) {
            $configs = $container->getExtensionConfig($this->getAlias());
            $atsUserconfig = $this->processConfiguration(new Configuration(), $configs);

            $config = [
                'db_driver' => 'mongodb',
                'client_class' => 'ATS\UserBundle\Document\Client',
                'access_token_class' => 'ATS\UserBundle\Document\AccessToken',
                'refresh_token_class' => 'ATS\UserBundle\Document\RefreshToken',
                'auth_code_class' => 'ATS\UserBundle\Document\AuthCode',
                'service' => [
                    'user_provider' => 'ATS\UserBundle\Provider\UserProvider',
                    'options' => [
                        'supported_scopes' => 'user admin super_admin',
                    ],
                ],
            ];
            if (isset($atsUserconfig['scopes']) === true) {
                $config['service']['options']['supported_scopes'] = $atsUserconfig['scopes'];
            }
            if (isset($atsUserconfig['user_provider']) === true) {
                $config['service']['user_provider'] = $atsUserconfig['user_provider'];
            }

            $container->prependExtensionConfig('fos_oauth_server', $config);
        }
    }
}
