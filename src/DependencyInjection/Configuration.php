<?php declare(strict_types=1);

namespace ATS\UserBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Configuration
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();

        $rootNode = $treeBuilder->root('ats_user');

        $rootNode
            ->children()

                ->enumNode('password_strength')
                    ->values(['none', 'weak', 'low', 'medium', 'strong'])->defaultValue('none')
                ->end()

                ->booleanNode('password_reuse')
                    ->defaultTrue()
                ->end()

                ->scalarNode('user_class')
                    ->defaultValue('ATS\UserBundle\Document\User')
                ->end()

                ->scalarNode('user_provider')
                    ->defaultValue('ATS\UserBundle\Provider\UserProvider')
                ->end()

                ->scalarNode('scopes')->end()

                ->arrayNode('roles_definition')
                    ->scalarPrototype()->end()
                    ->defaultValue(['ROLE_USER', 'ROLE_ADMIN', 'ROLE_SUPER_ADMIN'])
                ->end()

                ->arrayNode('ip_whitelist')
                    ->scalarPrototype()->end()
                    ->defaultValue([])
                ->end()

                ->arrayNode('ip_blacklist')
                    ->scalarPrototype()->end()
                    ->defaultValue([])
                ->end()

                ->arrayNode('routing')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('host')
                            ->defaultValue('127.0.0.1')
                        ->end()
                        ->scalarNode('prefix')
                            ->defaultValue('user')
                        ->end()
                        ->scalarNode('scheme')
                            ->defaultValue('http')
                        ->end()
                    ->end()
                ->end()

                ->arrayNode('routes')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('oauth_token')
                            ->defaultValue('/token')
                        ->end()
                        ->scalarNode('register')
                            ->defaultValue('/register')
                        ->end()
                        ->scalarNode('reset_password')
                            ->defaultValue('/reset-password')
                        ->end()
                        ->scalarNode('change_password')
                            ->defaultValue('/change-password')
                        ->end()
                    ->end()
                ->end()

                ->arrayNode('template')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('reset_password')
                            ->defaultValue('ATSUserBundle:Email:reset-password.html.twig')
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('reset_password')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('callback_uri')
                            ->defaultValue('http://127.0.0.1/user/new-password')
                        ->end()
                        ->scalarNode('token_parameter')
                            ->defaultValue('reset-token')
                        ->end()
                        ->scalarNode('subject')
                            ->defaultValue('Password reset')
                        ->end()
                        ->scalarNode('sender_name')
                            ->defaultValue('ATS')
                        ->end()
                        ->scalarNode('sender_email')
                            ->defaultValue('no-reply@ats-digital.com')
                        ->end()
                        ->booleanNode('save')
                            ->defaultTrue()
                        ->end()
                    ->end()
                ->end()

            ->end();


        return $treeBuilder;
    }
}
