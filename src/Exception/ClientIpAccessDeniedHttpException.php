<?php declare(strict_types=1);

namespace ATS\UserBundle\Exception;

use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * ClientIpAccessDeniedHttpException
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
class ClientIpAccessDeniedHttpException extends HttpException
{
    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct(403, 'Client Ip Address Unauthorized.', null, [], 0);
    }
}
