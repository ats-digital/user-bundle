<?php declare(strict_types=1);

namespace ATS\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ATSUserBundle extends Bundle
{
}
