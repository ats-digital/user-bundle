<?php declare (strict_types = 1);

namespace ATS\UserBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Question\Question;
use ATS\CoreBundle\Service\Util\StringWrapper;
use ATS\UserBundle\Util\PasswordUtils;
use ATS\UserBundle\Document\User;
use Psr\Log\LoggerInterface;
use Monolog\Logger;

/**
 * Change Password Command
 *
 * @author Elyes MANAI <emanai@ats-digital.com>
 */
class ChangePasswordCommand extends ContainerAwareCommand
{
    const CMD_NAME = 'ats:user:change-password';
    const CMD_DESC = 'Change password.';

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var PasswordUtils
     */
    private $passwordUtils;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var bool
     */
    private $isGeneratedPassword = false;

    /**
     * Constructor
     *
     * @param LoggerInterface $logger
     * @param PasswordUtils   $passwordUtils
     * @param ValidatorInterface $validator
     */
    public function __construct(LoggerInterface $logger, PasswordUtils $passwordUtils, ValidatorInterface $validator)
    {
        parent::__construct();
        $this->logger = $logger;
        $this->passwordUtils = $passwordUtils;
        $this->validator = $validator;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName(self::CMD_NAME)
            ->setDescription(self::CMD_DESC)
            ->setDefinition(
                array(
                    new InputArgument('username', InputArgument::REQUIRED, 'The username'),
                    new InputArgument('password', InputArgument::REQUIRED, 'The password'),
                    )
            )
            ->setHelp(
                <<<EOT
The <info<ats:user:change-password</info> command changes user's password:

  <info>php bin/console ats:user:change-password username </info>

The interactive shell will first ask you for a password.

you can alternatively specify the password as second argument:

  <info>php bin/console ats:user:change-password username password</info>

EOT
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $username = StringWrapper::stringify($input->getArgument('username'));
        $password = StringWrapper::stringify($input->getArgument('password'));

        $userManager = $this->getContainer()->get('ats_user.manager.user_manager');
        $user = $userManager->changePassword($username, $password);

        if ($user instanceof User) {
            $errors = $this->validator->validate($user, null, ['password.update']);
            if (count($errors) > 0) {
                throw new \Exception($errors[0]->getMessage());
            }
            $userManager->update($user);
            $message = sprintf('Change password for user <comment>%s</comment> succeeded', $username);
            $this->logger->log(
                Logger::INFO,
                'password changed',
                [
                    'command' => self::CMD_NAME,
                    'username' => $username,
                ]
            );
            if ($this->isGeneratedPassword === true) {
                $message .= sprintf(' with new password <comment>%s</comment>', $password);
            }
        } else {
            $message = sprintf('User with username <comment>%s</comment> does not exist', $username);
            $this->logger->log(
                Logger::WARNING,
                'password change failed, username does not exist.',
                [
                    'command' => self::CMD_NAME,
                    'username' => $username,
                ]
            );
        }
        $output->writeln($message);
    }

    /**
     * {@inheritdoc}
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $helper = $this->getHelper('question');

        if ($input->getArgument('username') === null) {
            $question = new Question('<info>Please choose a username:</info>');
            $question->setValidator(
                function ($username) {
                    if ($username === null || trim($username) === "") {
                        throw new \Exception('Username can not be empty');
                    }

                    return $username;
                }
            );

            $username = $helper->ask($input, $output, $question);
            $input->setArgument('username', $username);
        }

        if ($input->getArgument('password') === null) {
            $question = new Question('<info>Please choose a new password (empty for random):</info>');
            $userManager = $this->getContainer()->get('ats_user.manager.user_manager');
            $username = StringWrapper::stringify($input->getArgument('username'));
            $question->setValidator(
                function ($password) use ($userManager, $username) {
                    if ($password === null || trim($password) === "") {
                        $passwordStrength = $this->getContainer()->getParameter('ats_user.password_strength');
                        $password = $this->passwordUtils->generateRandom($passwordStrength);
                        $this->isGeneratedPassword = true;
                    }

                    $user = $userManager->changePassword($username, $password);

                    $errors = $this->validator->validate($user, null, ['password.update']);
                    if (count($errors) > 0) {
                        throw new \Exception($errors[0]->getMessage());
                    }

                    return $password;
                }
            );

            $password = $helper->ask($input, $output, $question);
            $input->setArgument('password', $password);
        }
    }
}
