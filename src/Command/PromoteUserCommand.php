<?php declare (strict_types = 1);

namespace ATS\UserBundle\Command;

use ATS\CoreBundle\Service\Util\StringWrapper;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Psr\Log\LoggerInterface;
use Monolog\Logger;

/**
 * Promote User
 *
 * @author Elyes MANAI <emanai@ats-digital.com>
 */
class PromoteUserCommand extends ContainerAwareCommand
{
    const CMD_NAME = 'ats:user:promote';
    const CMD_DESC = 'Promote a user.';

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Constructor
     *
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        parent::__construct();
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName(self::CMD_NAME)
            ->setDescription(self::CMD_DESC)
            ->setDefinition(
                [
                    new InputArgument('username', InputArgument::REQUIRED, 'The username'),
                    new InputArgument('role', InputArgument::REQUIRED, 'The user role'),
                ]
            )
            ->setHelp(
                <<<EOT
The <info<ats:user:promote</info> command promote user's role:

  <info>php bin/console ats:user:promote username </info>

The interactive shell will first ask you for the new role to add.

you can alternatively specify the role as second argument:

  <info>php bin/console ats:user:promote username role</info>

EOT
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $username = StringWrapper::stringify($input->getArgument('username'));
        $role = StringWrapper::stringify($input->getArgument('role'));

        $userManager = $this->getContainer()->get('ats_user.manager.user_manager');
        $userClass = $this->getContainer()->getParameter('ats_user.user_class');

        $user = $userManager->promote($username, $role);
        if ($user instanceof $userClass) {
            $message = sprintf(
                '%sUser <comment>%s</comment> promoted with role <comment>%s</comment>',
                PHP_EOL,
                $username,
                $role
            );
            $this->logger->log(
                Logger::INFO,
                'user promoted',
                [
                    'command' => self::CMD_NAME,
                    'username' => $username,
                    'role' => $role
                ]
            );
        } else {
            $message = sprintf(
                '%sUser <comment>%s</comment> does not exits or role <comment>%s</comment> is not supported',
                PHP_EOL,
                $username,
                $role
            );
            $this->logger->log(
                Logger::WARNING,
                'user promote failed, username does not exist or role not supported',
                [
                    'command' => self::CMD_NAME,
                    'username' => $username,
                    'role' => $role
                ]
            );
        }
        $output->writeln($message);
    }

    /**
     * {@inheritdoc}
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $helper = $this->getHelper('question');
        $rolesDefinition = $this->getContainer()->getParameter('ats_user.roles_definition');

        if ($input->getArgument('username') === null) {
            $question = new Question('<info>Please choose a username:</info>');
            $question->setValidator(
                function ($username) {
                    if ($username === null || trim($username) === "") {
                        throw new \Exception('Username can not be empty');
                    }

                    return $username;
                }
            );

            $username = $helper->ask($input, $output, $question);
            $input->setArgument('username', $username);
        }

        if ($input->getArgument('role') === null) {
            $question = new Question('<info>Please choose a role to add:</info>');
            $question->setValidator(
                function ($role) use ($rolesDefinition) {
                    if ($role === null || trim($role) === "") {
                        throw new \Exception('Role can not be empty');
                    }

                    if (in_array($role, $rolesDefinition) === false) {
                        throw new \Exception(
                            sprintf(
                                'Role "%s" not supported please pick one from the following list: %s',
                                $role,
                                StringWrapper::stringify($rolesDefinition)
                            )
                        );
                    }

                    return $role;
                }
            );

            $role = $helper->ask($input, $output, $question);
            $input->setArgument('role', $role);
        }
    }
}
