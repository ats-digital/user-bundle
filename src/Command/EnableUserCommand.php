<?php declare (strict_types = 1);

namespace ATS\UserBundle\Command;

use ATS\CoreBundle\Service\Util\StringWrapper;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use ATS\UserBundle\Document\User;
use Psr\Log\LoggerInterface;
use Monolog\Logger;

/**
 * Enable User Command
 *
 * @author Wajih WERIEMI <wajih@insideboard.com>
 */
class EnableUserCommand extends ContainerAwareCommand
{
    const CMD_NAME = 'ats:user:enable';
    const CMD_DESC = 'Enable a user.';

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Constructor
     *
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        parent::__construct();
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName(self::CMD_NAME)
            ->setDescription(self::CMD_DESC)
            ->setDefinition(
                array(
                    new InputArgument('username', InputArgument::REQUIRED, 'The username'),
                    )
            )
            ->setHelp(
                <<<EOT
The <info<ats:user:enable</info> command enable a user:

  <info>php bin/console ats:user:enable username </info>

EOT
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $username = StringWrapper::stringify($input->getArgument('username'));

        $userManager = $this->getContainer()->get('ats_user.manager.user_manager');
        $user = $userManager->loadUserByUsernameOrEmail($username);
        if ($user instanceof User) {
            $user = $userManager->activate($user);
            $message = sprintf('%sUser <comment>%s</comment> enabled', PHP_EOL, $username);
            $this->logger->log(
                Logger::INFO,
                'user enabled',
                [
                    'command' => self::CMD_NAME,
                    'username' => $username,
                ]
            );
        } else {
            $message = sprintf(
                '%sUser with username <comment>%s</comment> does not exist',
                PHP_EOL,
                $username
            );
            $this->logger->log(
                Logger::WARNING,
                'failed to enable user, username does not exist.',
                [
                    'command' => self::CMD_NAME,
                    'username' => $username,
                ]
            );
        }
        $output->writeln($message);
    }

    /**
     * {@inheritdoc}
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $helper = $this->getHelper('question');

        if ($input->getArgument('username') === null) {
            $question = new Question('<info>Please choose a username:</info>');
            $question->setValidator(
                function ($username) {
                    if ($username === null || trim($username) === "") {
                        throw new \Exception('Username can not be empty');
                    }

                    return $username;
                }
            );

            $username = $helper->ask($input, $output, $question);
            $input->setArgument('username', $username);
        }
    }
}
