<?php declare (strict_types = 1);

namespace ATS\UserBundle\Command;

use Symfony\Component\Config\Definition\Exception\MongoDuplicateKeyException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Question\Question;
use ATS\CoreBundle\Service\Util\StringWrapper;
use Symfony\Component\Console\Helper\Table;
use Psr\Log\LoggerInterface;
use Monolog\Logger;

/**
 * Create Client Command
 *
 * @author Wajih WERIEMI <wajih@insideboard.com>
 */
class CreateClientCommand extends ContainerAwareCommand
{
    const CMD_NAME = 'ats:user:client:create';
    const CMD_DESC = 'Create a client.';

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Constructor
     *
     * @param LoggerInterface    $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        parent::__construct();
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName(self::CMD_NAME)
            ->setDescription(self::CMD_DESC)
            ->setDefinition(
                [
                    new InputArgument('name', InputArgument::REQUIRED, 'The client name'),
                ]
            )
            ->setHelp(
                <<<EOT
The <info>ats:user:client:create</info> command creates a client:

  <info>php bin/console ats:user:client:create </info>

This interactive shell will ask you for a name.

You can alternatively specify the name as a first argument:

  <info>php bin/console ats:user:client:create name</info>

EOT
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = StringWrapper::stringify($input->getArgument('name'));

        $clientManager = $this->getContainer()->get('ats_user.manager.client_manager');

        try {
            $clientManager->create($name);
            $newClient = $clientManager->getOneBy(["name" => $name]);
            $this->logger->log(Logger::INFO, 'client created', ['command' => self::CMD_NAME, 'name' => $name]);
            $message = sprintf('%sCreated client', PHP_EOL);
            $output->writeln($message);

            $outputTable = new Table($output);
            $outputTable
                ->setHeaders(['name', 'client_id', 'client_secret'])
                ->setRows([[$name, $newClient->getClientId(), $newClient->getClientSecret()]]);
            $outputTable->render();
        } catch (\MongoDuplicateKeyException $e) {
            $message = sprintf('%s<error>Client "%s" alerady exists</error>', PHP_EOL, $name);
            $this->logger->log(Logger::NOTICE, 'client aleready exists', ['command' => self::CMD_NAME, 'name' => $name]);
            $output->writeln($message);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $helper = $this->getHelper('question');

        if ($input->getArgument('name') === null) {
            $question = new Question('<info>Please choose a name:</info>', null);
            $question->setValidator(
                function ($name) {
                    if ($name === null || trim($name) === "") {
                        throw new \Exception('Name can not be empty', 1);
                    }

                    return $name;
                }
            );
            $question->setMaxAttempts(3);

            $name = $helper->ask($input, $output, $question);
            $input->setArgument('name', $name);
        }
    }
}
