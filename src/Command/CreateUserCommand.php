<?php declare (strict_types = 1);

namespace ATS\UserBundle\Command;

use Symfony\Component\Config\Definition\Exception\MongoDuplicateKeyException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;
use ATS\UserBundle\Validator\Constraints as Assert;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Question\Question;
use ATS\CoreBundle\Service\Util\StringWrapper;
use ATS\UserBundle\Util\PasswordUtils;
use Psr\Log\LoggerInterface;
use Monolog\Logger;

/**
 * Create User Command
 *
 * @author Wajih WERIEMI <wajih@insideboard.com>
 */
class CreateUserCommand extends ContainerAwareCommand
{
    const CMD_NAME = 'ats:user:create';
    const CMD_DESC = 'Create a user.';

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var PasswordUtils
     */
    private $passwordUtils;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var bool
     */
    private $isGeneratedPassword = false;

    /**
     * Constructor
     *
     * @param LoggerInterface    $logger
     * @param PasswordUtils      $passwordUtils
     * @param ValidatorInterface $validator
     */
    public function __construct(LoggerInterface $logger, PasswordUtils $passwordUtils, ValidatorInterface $validator)
    {
        parent::__construct();
        $this->logger = $logger;
        $this->passwordUtils = $passwordUtils;
        $this->validator = $validator;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName(self::CMD_NAME)
            ->setDescription(self::CMD_DESC)
            ->setDefinition(
                [
                    new InputArgument('username', InputArgument::REQUIRED, 'The username'),
                    new InputArgument('email', InputArgument::REQUIRED, 'The email'),
                    new InputArgument('password', InputArgument::OPTIONAL, 'The password'),
                    new InputOption('activate', 'a', InputOption::VALUE_NONE, 'Activate the user on creation')
                ]
            )
            ->setHelp(
                <<<EOT
The <info>ats:user:create</info> command creates a user:

  <info>php bin/console ats:user:create username</info>

This interactive shell will ask you for a an email and then a password.

You can alternatively specify the email and password as the second and third arguments:

  <info>php bin/console ats:user:create username username@exemple.com password</info>

EOT
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $username = StringWrapper::stringify($input->getArgument('username'));
        $email = StringWrapper::stringify($input->getArgument('email'));
        $password = StringWrapper::stringify($input->getArgument('password'));
        $activate = (bool) $input->getOption('activate');

        $passwordStrengthConstraint = new Assert\PasswordStrength();
        $errors = $this->validator->validate(
            $password,
            $passwordStrengthConstraint
        );
        if (count($errors) > 0) {
            throw new \Exception($errors[0]->getMessage());
        }

        $userManager = $this->getContainer()->get('ats_user.manager.user_manager');

        try {
            $userManager->create($username, $email, $password, $activate);
            $this->logger->log(Logger::INFO, 'user created', ['command' => self::CMD_NAME, 'username' => $username]);
            $message = sprintf('%sCreated user <comment>%s</comment>', PHP_EOL, $username);
            if ($this->isGeneratedPassword === true) {
                $message .= sprintf(' with password <comment>%s</comment>', $password);
            }
            $output->writeln($message);
        } catch (\MongoDuplicateKeyException $e) {
            $message = sprintf('%s<error>User with username "%s" alerady exists</error>', PHP_EOL, $username);
            $this->logger->log(Logger::NOTICE, 'user aleready exists', ['command' => self::CMD_NAME, 'username' => $username]);
            $output->writeln($message);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $helper = $this->getHelper('question');

        if ($input->getArgument('username') === null) {
            $question = new Question('<info>Please choose a username:</info>', null);
            $question->setValidator(
                function ($username) {
                    if ($username === null || trim($username) === "") {
                        throw new \Exception('Username can not be empty', 1);
                    }

                    return $username;
                }
            );
            $question->setMaxAttempts(3);

            $username = $helper->ask($input, $output, $question);
            $input->setArgument('username', $username);
        }

        if ($input->getArgument('email') === null) {
            $question = new Question('<info>Please choose an email:</info>', null);
            $question->setValidator(
                function ($email) {
                    if ($email === null || trim($email) === "") {
                        throw new \Exception('Email can not be empty');
                    }

                    return $email;
                }
            );
            $question->setMaxAttempts(3);
            $email = $helper->ask($input, $output, $question);
            $input->setArgument('email', $email);
        }

        if ($input->getArgument('password') === null) {
            $question = new Question('<info>Please choose a password (empty for random):</info>', null);
            $passwordStrengthConstraint = new Assert\PasswordStrength();
            $question->setValidator(
                function ($password) use ($passwordStrengthConstraint) {
                    if ($password === null || trim($password) === "") {
                        $passwordStrength = $this->getContainer()->getParameter('ats_user.password_strength');
                        $password = $this->passwordUtils->generateRandom($passwordStrength);
                        $this->isGeneratedPassword = true;
                    }

                    $errors = $this->validator->validate(
                        $password,
                        $passwordStrengthConstraint
                    );
                    if (count($errors) > 0) {
                        throw new \Exception($errors[0]->getMessage());
                    }

                    return $password;
                }
            );
            $password = $helper->ask($input, $output, $question);
            $input->setArgument('password', $password);
        }
    }
}
