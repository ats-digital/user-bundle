<?php declare (strict_types = 1);

namespace ATS\UserBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;
use ATS\UserBundle\Validator\Constraints as ATSAssert;

/**
 * User
 *
 * @ODM\Document(repositoryClass="ATS\UserBundle\Repository\UserRepository")
 * @ODM\HasLifecycleCallbacks
 * @JMS\ExclusionPolicy("all")
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
class User implements UserInterface, \Serializable
{
    const DEFAULT_ROLE = "ROLE_USER";

    /**
     * @var \MongoId
     *
     * @ODM\Id("strategy=auto")
     * @JMS\Type("string")
     * @JMS\Expose
     */
    protected $id;

    /**
     * @var ?string
     *
     * @Assert\NotNull(message="error.validation.null", groups={"registration", "update"})
     * @Assert\NotBlank(message="error.validation.blank", groups={"registration", "update"})
     * @ODM\Field(type="string")
     * @ODM\Index(name="$_username", unique=true)
     * @JMS\Type("string")
     * @JMS\Expose
     */
    protected $username;

    /**
     * @var ?string
     *
     * @Assert\NotNull(message="error.validation.null", groups={"registration", "update"})
     * @Assert\NotBlank(message="error.validation.blank", groups={"registration", "update"})
     * @Assert\Email(checkMX=true, message="error.validation.invalid", groups={"registration", "update"})
     * @ODM\Field(type="string")
     * @ODM\Index(name="$_email", unique=true)
     * @JMS\Type("string")
     * @JMS\Expose
     */
    protected $email;

    /**
     * @var string
     *
     * @ODM\Field(type="string")
     */
    protected $password;

    /**
     * @var ?string
     *
     * @ATSAssert\PasswordStrength(
     *     groups={"registration", "password.update"}
     * )
     * @JMS\Type("string")
     * @JMS\Expose
     */
    protected $plainPassword;

    /**
     * @var array
     *
     * @ODM\Field(type="hash")
     */
    protected $passwordHistory;

    /**
     * @var array
     *
     * @ATSAssert\PasswordReuse(
     *     groups={"registration", "password.update"}
     * )
     */
    protected $plainPasswordHistory = [];

    /**
     * @var ?string
     *
     * @ODM\Field(type="string")
     */
    protected $salt;

    /**
     * @var bool
     *
     * @ODM\Field(type="boolean")
     * @JMS\Type("boolean")
     * @JMS\Expose
     */
    protected $active;

    /**
     * @var ?\DateTime
     *
     * @ODM\Field(type="date")
     */
    protected $expireAt;

    /**
     * @var array
     *
     * @ODM\Field(type="hash")
     * @JMS\Type("array<string>")
     * @JMS\Expose
     */
    protected $roles;

    /**
     * @var ?string
     *
     * @ODM\Field(type="string")
     * @JMS\Type("string")
     * @JMS\Expose
     */
    protected $resetToken;

    /**
     * @var ?\DateTime
     *
     * @ODM\Field(type="date")
     * @JMS\Type("DateTime")
     * @JMS\Expose
     */
    protected $resetTokenExpireAt;

    /**
     * Get id
     *
     * @return \MongoId
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param ?string $username
     *
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return ?string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * {@inheritDoc}
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set salt
     *
     * @param ?string $salt
     *
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get salt
     *
     * @return ?string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set active
     *
     * @param bool $active
     *
     * @return User
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set plainPassword
     *
     * @param ?string $plainPassword
     *
     * @return User
     */
    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    /**
     * Set plainPasswordHistory
     *
     * @param ?string $plainPassword
     *
     * @return User
     */
    public function setPlainPasswordHistory($plainPassword)
    {
        $this->plainPasswordHistory = $this->passwordHistory;
        $this->plainPasswordHistory[] = $plainPassword;

        return $this;
    }

    /**
     * Get plainPassword
     *
     * @return ?string
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * Set passwordHistory
     *
     * @param array $passwordHistory
     *
     * @return User
     */
    public function setPasswordHistory(array $passwordHistory)
    {
        $this->passwordHistory = $passwordHistory;

        return $this;
    }

    /**
     * Add password
     *
     * @param string $password
     *
     * @return User
     */
    public function addPasswordHistory($password)
    {
        if ($this->passwordHistory === null || in_array($password, $this->passwordHistory) === false) {
            $this->passwordHistory[] = $password;
        }

        return $this;
    }

    /**
     * Get passwordHistory
     *
     * @return ?array
     */
    public function getPasswordHistory()
    {
        return $this->passwordHistory;
    }

    /**
     * Activates a user
     *
     * @return User
     */
    public function activate()
    {
        return $this->setActive(true);
    }

    /**
     * Deactivates a user
     *
     * @return User
     */
    public function deactivate()
    {
        return $this->setActive(false);
    }

    /**
     * Set expireAt
     *
     * @param \DateTime $expireAt
     *
     * @return User
     */
    public function setExpireAt(\DateTime $expireAt)
    {
        $this->expireAt = $expireAt;

        return $this;
    }

    /**
     * Get expireAt
     *
     * @return \DateTime|null
     */
    public function getExpireAt()
    {
        return $this->expireAt;
    }

    /**
     * Set roles
     *
     * @param array $roles
     *
     * @return User
     */
    public function setRoles(array $roles = [])
    {
        if (count($roles) === 0) {
            return $this;
        }

        $this->roles = [self::DEFAULT_ROLE];

        foreach ($roles as $role) {
            if ($role === self::DEFAULT_ROLE) {
                continue;
            }
            array_push($this->roles, $role);
        }

        return $this;
    }

    /**
     * Get roles
     *
     * @return array
     */
    public function getRoles()
    {
        return $this->roles === null ? [self::DEFAULT_ROLE] : $this->roles;
    }

    /**
     * @param string $role
     *
     * @return bool
     */
    public function hasRole($role)
    {
        return in_array($role, $this->getRoles());
    }

    /**
     * Promote user roles
     *
     * @param string $role
     *
     * @return User
     */
    public function promote($role)
    {
        if ($role === self::DEFAULT_ROLE) {
            return $this;
        }

        if (in_array($role, $this->getRoles()) === false) {
            // could be better
            $this->roles = $this->getRoles();
            array_push($this->roles, $role);
        }

        return $this;
    }

    /**
     * Demote user roles
     *
     * @param string $role
     *
     * @return User
     */
    public function demote($role)
    {
        if ($role === self::DEFAULT_ROLE) {
            return $this;
        }

        $this->roles = array_diff($this->getRoles(), [$role]);

        return $this;
    }

    /**
     * Set resetToken
     *
     * @param string $resetToken
     *
     * @return User
     */
    public function setResetToken($resetToken)
    {
        $this->resetToken = $resetToken;

        return $this;
    }

    /**
     * Get resetToken
     *
     * @return string|null
     */
    public function getResetToken()
    {
        return $this->resetToken;
    }

    /**
     * Set resetTokenExpireAt
     *
     * @param \DateTime $resetTokenExpireAt
     *
     * @return User
     */
    public function setResetTokenExpireAt(\DateTime $resetTokenExpireAt)
    {
        $this->resetTokenExpireAt = $resetTokenExpireAt;

        return $this;
    }

    /**
     * Get resetTokenExpireAt
     *
     * @return \DateTime|null
     */
    public function getResetTokenExpireAt()
    {
        return $this->resetTokenExpireAt;
    }

    /**
     * Is resetToken non Expired
     *
     * @return bool
     */
    public function isResetTokenNonExpired()
    {
        if ($this->resetTokenExpireAt === null || $this->resetToken === null) {
            return false;
        }

        return $this->resetTokenExpireAt > (new \DateTime('now', new \DateTimeZone('etc/utc')));
    }

    /**
     * {@inheritDoc}
     */
    public function eraseCredentials()
    {
    }

    /**
     * Erease resetToken credentials
     *
     * @return User
     */
    public function eraseResetTokenCredentials()
    {
        $this->resetToken = null;
        $this->resetTokenExpireAt = null;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function isAccountNonExpired()
    {
        if ($this->expireAt === null) {
            return true;
        }

        return $this->expireAt > (new \DateTime('now', new \DateTimeZone('etc/utc')));
    }

    /**
     * {@inheritDoc}
     */
    public function isAccountNonLocked()
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function isCredentialsNonExpired()
    {
        return true;
    }

    /**
     * {@inheritDoc}
     *
     * @JMS\VirtualProperty()
     */
    public function isEnabled()
    {
        return $this->active;
    }

    /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(
            [
                $this->id,
                $this->username,
                $this->password,
                $this->salt,
                $this->active,
            ]
        );
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list(
            $this->id,
            $this->username,
            $this->password,
            $this->salt,
            $this->active
        ) = unserialize($serialized);
    }

    /**
     * Account expired
     *
     * @JMS\VirtualProperty(
     *   name="account_expired"
     * )
     */
    public function isAccountExpired()
    {
        return !$this->isAccountNonExpired();
    }

    /**
     * ResetToken expired
     *
     * @JMS\VirtualProperty(
     *   name="reset_token_expired"
     * )
     */
    public function isResetTokenExpired()
    {
        return !$this->isResetTokenNonExpired();
    }

    /**
     * Account locked
     *
     * @JMS\VirtualProperty(
     *   name="account_locked"
     * )
     */
    public function isAccountLocked()
    {
        return !$this->isAccountNonLocked();
    }

    /**
     * Credentials expired
     *
     * @JMS\VirtualProperty(
     *   name="credentials_expired"
     * )
     */
    public function isCredentialsExpired()
    {
        return !$this->isCredentialsNonExpired();
    }
}
