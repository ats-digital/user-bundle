<?php declare (strict_types = 1);

namespace ATS\UserBundle\Document;

use Symfony\Component\Security\Core\User\AdvancedUserInterface;

interface UserInterface extends AdvancedUserInterface
{
    /**
     * Set email
     *
     * @param ?string $email
     *
     * @return User
     */
    public function setEmail($email);

    /**
     * Get email
     *
     * @return ?string
     */
    public function getEmail();
}
