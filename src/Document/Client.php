<?php declare (strict_types = 1);

namespace ATS\UserBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use JMS\Serializer\Annotation as JMS;
use FOS\OAuthServerBundle\Document\Client as BaseClient;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Client
 *
 * @ODM\Document(repositoryClass="ATS\UserBundle\Repository\ClientRepository")
 * @ODM\HasLifecycleCallbacks
 * @JMS\ExclusionPolicy("all")
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
class Client extends BaseClient
{
    const GRANT_TYPE_PASSWORD = "password";
    const GRANT_TYPE_REFRESH_TOKEN = "refresh_token";

    /**
     * @var \MongoId
     *
     * @ODM\Id("strategy=auto")
     * @JMS\Expose
     */
    protected $id;

    /**
     * @var ?string
     *
     * @Assert\NotNull(message="error.validation.null")
     * @Assert\NotBlank(message="error.validation.blank")
     * @ODM\Field(type="string")
     * @ODM\Index(name="$_name", unique=true)
     * @JMS\Type("string")
     * @JMS\Expose
     */
    protected $name;

    /**
     * @var ?string
     *
     * @Assert\NotNull(message="error.validation.null")
     * @Assert\NotBlank(message="error.validation.blank")
     * @Assert\Url(protocols={"http", "https"}, message="error.validation.url")
     */
    protected $redirectUri;

    /**
     * Set name
     *
     * @param ?string $name
     *
     * @return Client
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return ?string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set redirectUri
     *
     * @param ?string $redirectUri
     *
     * @return Client
     */
    public function setRedirectUri($redirectUri)
    {
        $this->redirectUri = $redirectUri;

        return $this;
    }

    /**
     * Get redirectUri
     *
     * @return ?string
     */
    public function getRedirectUri()
    {
        return $this->redirectUri;
    }

    /**
     * Get client id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @JMS\Expose
     *
     * @return string
     */
    public function getClientId()
    {
        return $this->getPublicId();
    }

    /**
     * Get client secret
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @JMS\Expose
     *
     * @return string
     */
    public function getClientSecret()
    {
        return $this->getSecret();
    }

    /**
     * Get redirect urls
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @JMS\Expose
     *
     * @return array
     */
    public function getRedirectUrls()
    {
        return $this->getRedirectUris();
    }

    /**
     * Get grant types
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @JMS\Expose
     *
     * @return array
     */
    public function getGrantTypes()
    {
        return $this->getAllowedGrantTypes();
    }
}
