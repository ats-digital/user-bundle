<?php declare (strict_types = 1);

namespace ATS\UserBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use JMS\Serializer\Annotation as JMS;
use FOS\OAuthServerBundle\Document\AuthCode as BaseAuthCode;
use FOS\OAuthServerBundle\Model\ClientInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * AuthCode
 *
 * @ODM\Document(repositoryClass="ATS\UserBundle\Repository\AuthCodeRepository")
 * @ODM\HasLifecycleCallbacks
 * @JMS\ExclusionPolicy("all")
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
class AuthCode extends BaseAuthCode
{
    /**
     * @var \MongoId
     *
     * @ODM\Id("strategy=auto")
     * @JMS\Expose
     */
    protected $id;

    /**
     * @var ClientInterface
     *
     * @ODM\ReferenceOne(targetDocument="Client", storeAs="dbRef")
     * @JMS\Type("ATS\UserBundle\Document\Client")
     * @JMS\Expose
     */
    protected $client;

    /**
     * @var UserInterface
     *
     * @ODM\ReferenceOne(targetDocument="User", storeAs="dbRef", cascade="remove")
     * @JMS\Type("ATS\UserBundle\Document\User")
     * @JMS\Expose
     */
    protected $user;

    /**
     * Set client
     *
     * @param ClientInterface $client
     *
     * @return AuthCode
     */
    public function setClient(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * Get client
     *
     * @return ClientInterface
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set user
     *
     * @param UserInterface $user
     *
     * @return AuthCode
     */
    public function setUser(UserInterface $user)
    {
        $this->user = $user;
    }

    /**
     * Get user
     *
     * @return UserInterface
     */
    public function getUser()
    {
        return $this->user;
    }
}
