<?php declare (strict_types = 1);

namespace ATS\UserBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use JMS\Serializer\Annotation as JMS;
use FOS\OAuthServerBundle\Document\RefreshToken as BaseRefreshToken;
use FOS\OAuthServerBundle\Model\ClientInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * RefreshToken
 *
 * @ODM\Document(repositoryClass="ATS\UserBundle\Repository\RefreshTokenRepository")
 * @ODM\HasLifecycleCallbacks
 * @JMS\ExclusionPolicy("all")
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
class RefreshToken extends BaseRefreshToken
{
    /**
     * @var \MongoId
     *
     * @ODM\Id("strategy=auto")
     * @JMS\Expose
     */
    protected $id;

    /**
     * @var ClientInterface
     *
     * @ODM\ReferenceOne(targetDocument="Client", storeAs="dbRef")
     * @JMS\Type("ATS\UserBundle\Document\Client")
     * @JMS\Expose
     */
    protected $client;

    /**
     * @var UserInterface
     *
     * @ODM\ReferenceOne(targetDocument="User", storeAs="dbRef")
     * @JMS\Type("ATS\UserBundle\Document\User")
     * @JMS\Expose
     */
    protected $user;

    /**
     * Set client
     *
     * @param ClientInterface $client
     *
     * @return RefreshToken
     */
    public function setClient(ClientInterface $client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return ClientInterface
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set user
     *
     * @param UserInterface $user
     *
     * @return RefreshToken
     */
    public function setUser(UserInterface $user)
    {
        $this->user = $user;
    }

    /**
     * Get user
     *
     * @return UserInterface
     */
    public function getUser()
    {
        return $this->user;
    }
}
