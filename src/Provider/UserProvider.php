<?php declare (strict_types = 1);

namespace ATS\UserBundle\Provider;

use ATS\UserBundle\Manager\UserManager;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

class UserProvider implements UserProviderInterface
{
    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var string
     */
    private $userClass;

    /**
     * Construct
     *
     * @param UserManager $userManager
     * @param string      $userClass
     */
    public function __construct(UserManager $userManager, $userClass)
    {
        $this->userManager = $userManager;
        $this->userClass = $userClass;
    }

    /**
     * {@inheritDoc}
     */
    public function loadUserByUsername($username)
    {
        return $this->fetchUser($username);
    }

    /**
     * {@inheritDoc}
     */
    public function refreshUser(UserInterface $user)
    {
        if ($this->supportsClass(get_class($user)) === false) {
            throw new UnsupportedUserException(
                sprintf('Instances of "%s" are not supported.', get_class($user))
            );
        }

        $username = $user->getUsername();

        return $this->fetchUser($username);
    }

    /**
     * {@inheritDoc}
     */
    private function fetchUser($username)
    {
        $user = $this->userManager->loadUserByUsernameOrEmail($username);

        if ($user instanceof $this->userClass) {
            return $user;
        }
        throw new UsernameNotFoundException(
            sprintf('Username "%s" does not exist.', $username)
        );
    }

    /**
     * {@inheritDoc}
     */
    public function supportsClass($class)
    {
        return $this->userClass === $class;
    }
}
