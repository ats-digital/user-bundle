<?php declare (strict_types = 1);

namespace ATS\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ATS\CoreBundle\Controller\Rest\RestControllerTrait;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use ATS\UserBundle\Service\UserService;

/**
 * UserController
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
class UserController extends Controller
{
    use RestControllerTrait;

    /**
     * Register action
     *
     * @param Request     $request
     * @param UserService $userService
     *
     * @return JsonResponse
     */
    public function registerAction(Request $request, UserService $userService)
    {
        $response = $userService->create($request->getContent());

        return $this->renderResponse($response['content'], $response['statusCode']);
    }

    /**
     * Reset Password action
     *
     * @param Request     $request
     * @param UserService $userService
     *
     * @return JsonResponse
     */
    public function resetPasswordAction(Request $request, UserService $userService)
    {
        $response = $userService->resetPassword($request->getContent());

        return $this->renderResponse($response['content'], $response['statusCode']);
    }

    /**
     * Change Password action
     *
     * @param Request     $request
     * @param UserService $userService
     *
     * @return JsonResponse
     */
    public function changePasswordAction(Request $request, UserService $userService)
    {
        $response = $userService->changePassword($request->getContent());

        return $this->renderResponse($response['content'], $response['statusCode']);
    }
}
