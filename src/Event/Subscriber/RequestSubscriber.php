<?php declare(strict_types=1);

namespace ATS\UserBundle\Event\Subscriber;

use ATS\UserBundle\Exception\ClientIpAccessDeniedHttpException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Request Event Subscriber
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
class RequestSubscriber implements EventSubscriberInterface
{
    /**
     * @var array
     */
    private $ipWhitelist;

    /**
     * @var array
     */
    private $ipBlacklist;

    /**
     * Set lists
     *
     * @param array $ipWhitelist
     * @param array $ipBlacklist
     */
    public function setLists($ipWhitelist, $ipBlacklist)
    {
        $this->ipWhitelist = $ipWhitelist;
        $this->ipBlacklist = $ipBlacklist;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return [
           KernelEvents::REQUEST => [
               ['processRequest', 10],
           ]
        ];
    }

    /**
     * Process Request
     *
     * @param GetResponseEvent $event
     */
    public function processRequest(GetResponseEvent $event)
    {
        $clientIp = $event->getRequest()->getClientIp();
        if ($this->isAuthorizedIp($clientIp) === true) {
            return;
        }

        throw new ClientIpAccessDeniedHttpException();
    }

    /**
     * Is client ip authorized
     *
     * @param string|null $clientIp
     *
     * @return bool
     */
    private function isAuthorizedIp($clientIp)
    {
        if (empty($this->ipWhitelist) === true && empty($this->ipBlacklist) === true) {
            return true;
        }

        if (in_array($clientIp, $this->ipBlacklist) === true) {
            return false;
        }

        if (empty($this->ipWhitelist) === true || in_array($clientIp, $this->ipWhitelist) === true) {
            return true;
        }

        return false;
    }
}
