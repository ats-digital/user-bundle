<?php declare(strict_types=1);

namespace ATS\UserBundle\Event\Listener;

use Doctrine\ODM\MongoDB\Event\LoadClassMetadataEventArgs;

/**
 * ClassMetadataListener
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
class ClassMetadataListener
{
    const DEFAULT_USER_CLASS = 'ATS\UserBundle\Document\User';

    /**
     * @var string
     */
    private $userClass;

    /**
     * Constructor
     *
     * @param string $userClass
     */
    public function __construct($userClass)
    {
        $this->userClass = $userClass;
    }

    /**
     * Load class meta data
     *
     * @param LoadClassMetadataEventArgs $eventArgs
     */
    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs)
    {
        if ($this->userClass === self::DEFAULT_USER_CLASS) {
            return;
        }

        $classMetadata = $eventArgs->getClassMetadata();

        if ($classMetadata->getNamespace() !== "ATS\UserBundle\Document") {
            return;
        }

        if ($classMetadata->getCollection() === "AccessToken"
            || $classMetadata->getCollection() === "RefreshToken"
            || $classMetadata->getCollection() === "AuthCode"
        ) {
            $mapping = $classMetadata->fieldMappings['user'];
            $mapping['targetDocument'] = $this->userClass;
            $classMetadata->mapField($mapping);
        }
    }
}
