<?php declare(strict_types=1);

namespace ATS\UserBundle\Util;

use ATS\UserBundle\Validator\Constraints\PasswordStrengthValidator;

/**
 * PasswordUtils
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
class PasswordUtils
{
    /**
     * Generate random password
     *
     * @param string $strength
     * @param int    $length
     *
     * @return string
     */
    public function generateRandom($strength, $length = PasswordStrengthValidator::MINIMUM_LENGTH)
    {
        if ($length < PasswordStrengthValidator::MINIMUM_LENGTH) {
            $length = PasswordStrengthValidator::MINIMUM_LENGTH;
        }

        $random = '';
        switch ($strength) {
            case PasswordStrengthValidator::WEAK_STRENGTH:
                $random = substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789&@?!(){}/+-*~#_'), 0, $length);
                break;
            case PasswordStrengthValidator::LOW_STRENGTH:
                $capitalLetter = substr(str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, 1);
                $rest = substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789&@?!(){}/+-*~#_'), 0, $length - 1);
                $random = $capitalLetter . $rest;
                break;
            case PasswordStrengthValidator::MEDIUM_STRENGTH:
                $capitalLetter = substr(str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, 1);
                $numeric = substr(str_shuffle('0123456789'), 0, 1);
                $rest = substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789&@?!(){}/+-*~#_'), 0, $length - 2);
                $random = $capitalLetter . $numeric . $rest;
                break;
            case PasswordStrengthValidator::STRONG_STRENGTH:
                $capitalLetter = substr(str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, 1);
                $numeric = substr(str_shuffle('0123456789'), 0, 1);
                $symbol = substr(str_shuffle('&@?!(){}/+-*~#_'), 0, 1);
                $rest = substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789&@?!(){}/+-*~#_'), 0, $length - 3);
                $random = $capitalLetter . $numeric . $symbol . $rest;
                break;

            default:
                $random = substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789&@?!(){}/+-*~#_'), 0, $length);
                break;
        }

        return substr(str_shuffle($random), 0, $length);
    }
}
