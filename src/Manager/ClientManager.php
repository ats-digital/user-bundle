<?php declare (strict_types = 1);

namespace ATS\UserBundle\Manager;

use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use FOS\OAuthServerBundle\Document\ClientManager as BaseClientManager;
use ATS\CoreBundle\Manager\AbstractManager;
use ATS\UserBundle\Document\Client;

/**
 * ClientManager
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
class ClientManager extends AbstractManager
{
    /**
     * @var BaseClientManager
     */
    protected $baseClientManager;

    /**
     * @var string
     */
    protected $redirectUri;

    /**
     * Constructor
     *
     * @param ManagerRegistry $managerRegistry
     * @param string          $documentClass
     * @param string          $managerName
     */
    public function __construct(ManagerRegistry $managerRegistry, $documentClass = Client::class, $managerName = null)
    {
        parent::__construct($managerRegistry, $documentClass, $managerName);
    }

    /**
     * Set baseClientManager
     *
     * @param BaseClientManager $baseClientManager
     */
    public function setBaseClientManager(BaseClientManager $baseClientManager)
    {
        $this->baseClientManager = $baseClientManager;
    }

    /**
     * Set redirectUri
     *
     * @param string $scheme
     * @param string $host
     */
    public function setRedirectUri($scheme, $host)
    {
        $this->redirectUri = sprintf('%s://%s', $scheme, $host);
    }

    /**
     * Get baseClientManager
     *
     * @return BaseClientManager
     */
    public function getBaseClientManager()
    {
        return $this->baseClientManager;
    }

    /**
     * Create
     *
     * @param string $name
     *
     * @return \MongoId
     */
    public function create($name)
    {
        /** @var Client */
        $client = $this->baseClientManager->createClient();

        $client->setName($name);
        $client->setRedirectUri($this->redirectUri);

        return $this->update($client);
    }

    /**
     * {@inheritDoc}
     */
    public function update($document)
    {
        if ($document->getRedirectUri() !== null) {
            $document->setRedirectUris(
                [
                    $document->getRedirectUri(),
                ]
            );
            $document->setRedirectUri(null);
            $document->setAllowedGrantTypes(
                [
                    Client::GRANT_TYPE_PASSWORD,
                    Client::GRANT_TYPE_REFRESH_TOKEN,
                ]
            );
        }

        return parent::update($document);
    }

    /*
     * {@inheritDoc}
     */
    public function batchUpdate(array $documents)
    {
        foreach ($documents as $document) {
            if ($document->getRedirectUri() !== null) {
                $document->setRedirectUris(
                    [
                        $document->getRedirectUri(),
                    ]
                );
                $document->setRedirectUri(null);
                $document->setAllowedGrantTypes(
                    [
                        Client::GRANT_TYPE_PASSWORD,
                        Client::GRANT_TYPE_REFRESH_TOKEN,
                    ]
                );
            }
        }

        parent::batchUpdate($documents);
    }
}
