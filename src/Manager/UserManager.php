<?php declare (strict_types = 1);

namespace ATS\UserBundle\Manager;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use ATS\CoreBundle\Manager\AbstractManager;

/**
 * UserManager
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
class UserManager extends AbstractManager
{
    /**
     * @var UserPasswordEncoderInterface
     */
    protected $encoder;

    /**
     * @var string
     */
    protected $userClass;

    /**
     * @var array
     */
    protected $rolesDefinition;

    /**
     * Constructor
     *
     * @param ManagerRegistry $managerRegistry
     * @param string          $userClass
     * @param array           $rolesDefinition
     * @param string          $managerName
     */
    public function __construct(ManagerRegistry $managerRegistry, $userClass, array $rolesDefinition, $managerName = null)
    {
        $this->userClass = $userClass;
        $this->rolesDefinition = $rolesDefinition;
        parent::__construct($managerRegistry, $userClass, $managerName);
    }

    /**
     * Set encoder
     *
     * @param UserPasswordEncoderInterface $encoder
     */
    public function setEncoder(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    /**
     * Load user by username or email
     *
     * @param string $criteria
     *
     * @return AdvancedUserInterface
     */
    public function loadUserByUsernameOrEmail($criteria)
    {
        return $this->getDocumentRepository()->loadUserByUsernameOrEmail($criteria);
    }

    /**
     * Load user by resetToken
     *
     * @param string $resetToken
     *
     * @return AdvancedUserInterface
     */
    public function loadUserByResetToken($resetToken)
    {
        return $this->getDocumentRepository()->loadUserByResetToken($resetToken);
    }

    /**
     * Create user
     *
     * @param string $username
     * @param string $email
     * @param string $plainPassword
     * @param bool   $active
     *
     * @return AdvancedUserInterface
     */
    public function create($username, $email, $plainPassword, $active = false)
    {
        $user = new $this->userClass();
        $user->setUsername($username);
        $user->setEmail($email);
        $user->setPlainPassword($plainPassword);
        $user->setActive($active);

        $this->update($user);

        return $user;
    }

    /**
     * Activate user
     *
     * @param AdvancedUserInterface $user
     *
     * @return AdvancedUserInterface
     */
    public function activate(AdvancedUserInterface $user)
    {
        $user->activate();
        $this->update($user);

        return $user;
    }

    /**
     * Deactivate user
     *
     * @param AdvancedUserInterface $user
     *
     * @return AdvancedUserInterface
     */
    public function deactivate(AdvancedUserInterface $user)
    {
        $user->deactivate();
        $this->update($user);

        return $user;
    }

    /**
     * Reset user password
     *
     * @param string $username
     *
     * @throws NotFoundHttpException
     *
     * @return AdvancedUserInterface|null
     */
    public function resetPassword($username)
    {
        $user = $this->loadUserByUsernameOrEmail($username);

        if ($this->supportClass($user) === false) {
            throw new NotFoundHttpException('reset_password.username.not_exist');
        }

        if ($user->isResetTokenExpired() === true) {
            $token = bin2hex(random_bytes(32));
            $expireAt = new \DateTime('+2 hour', new \DateTimeZone('Etc/Utc'));
            $user->setResetToken($token);
            $user->setResetTokenExpireAt($expireAt);

            $this->update($user);
        }

        return $user;
    }

    /**
     * Changes the password for the given user.
     *
     * @param string $username
     * @param string $plainPassword
     *
     * @return AdvancedUserInterface|null
     */
    public function changePassword($username, $plainPassword)
    {
        $user = $this->loadUserByUsernameOrEmail($username);
        if ($user !== null) {
            $user->activate();
            $user->setPlainPassword($plainPassword);
            $user->setPlainPasswordHistory($plainPassword);
        }

        return $user;
    }

    /**
     * Changes the password for the given user by resetToken.
     *
     * @param string $resetToken
     * @param string $plainPassword
     *
     * @throws NotFoundHttpException
     * @throws BadRequestHttpException
     *
     * @return AdvancedUserInterface|null
     */
    public function changePasswordByResetToken($resetToken, $plainPassword)
    {
        $user = $this->loadUserByResetToken($resetToken);

        if ($this->supportClass($user) === false) {
            throw new NotFoundHttpException('change_password.token.not_exist');
        }

        if ($user->isResetTokenExpired() === true) {
            throw new BadRequestHttpException('reset_password.token.expired');
        }

        $user->eraseResetTokenCredentials();
        $user->activate();
        $user->setPlainPassword($plainPassword);
        $user->setPlainPasswordHistory($plainPassword);

        return $user;
    }

    /**
     * Promote the given user
     *
     * @param string $username
     * @param string $role
     *
     * @return AdvancedUserInterface|null
     */
    public function promote($username, $role)
    {
        if (in_array($role, $this->rolesDefinition) === false) {
            return null;
        }

        $user = $this->loadUserByUsernameOrEmail($username);

        if ($this->supportClass($user) === true) {
            $user = $user->promote($role);
            $this->update($user);
        }

        return $user;
    }

    /**
     * Demote the given user
     *
     * @param string $username
     * @param string $role
     *
     * @return AdvancedUserInterface|null
     */
    public function demote($username, $role)
    {
        if (in_array($role, $this->rolesDefinition) === false) {
            return null;
        }

        $user = $this->loadUserByUsernameOrEmail($username);

        if ($this->supportClass($user) === true) {
            $user = $user->demote($role);
            $this->update($user);
        }

        return $user;
    }

    /**
     * {@inheritDoc}
     */
    public function update($document)
    {
        if ($document->getPlainPassword() !== null) {
            $password = $this->encoder->encodePassword($document, $document->getPlainPassword());
            $document->setPassword($password);
            $document->addPasswordHistory($password);
            $document->setPlainPassword(null);
        }

        return parent::update($document);
    }

    /*
     * {@inheritDoc}
     */
    public function batchUpdate(array $documents)
    {
        foreach ($documents as $document) {
            if ($document->getPlainPassword() !== null) {
                $password = $this->encoder->encodePassword($document, $document->getPlainPassword());
                $document->setPassword($password);
                $document->addPasswordHistory($password);
                $document->setPlainPassword(null);
            }
        }

        parent::batchUpdate($documents);
    }

    /**
     * Support class
     *
     * @param AdvancedUserInterface|null $user
     *
     * @return bool
     */
    protected function supportClass(?AdvancedUserInterface $user)
    {
        return $user !== null && get_class($user) === $this->userClass;
    }
}
