<?php declare (strict_types = 1);

namespace ATS\UserBundle\Dev\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ATS\CoreBundle\Controller\Rest\RestControllerTrait;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use ATS\UserBundle\Service\ClientService;

/**
 * OAuthController
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
class OAuthController extends Controller
{
    use RestControllerTrait;

    /**
     * Clients action
     *
     * @param Request       $request
     * @param ClientService $clientService
     *
     * @return JsonResponse
     */
    public function clientsAction(Request $request, ClientService $clientService)
    {
        return $this->renderResponse($clientService->getClients(), Response::HTTP_OK, ['list']);
    }

    /**
     * Create client action
     *
     * @param Request       $request
     *
     * @return JsonResponse
     */
    public function createClientAction(Request $request, ClientService $clientService)
    {
        $serviceResponse = $clientService->create(
            $request->request->get('name')
        );

        $statusCode = $serviceResponse['status'] === false ? Response::HTTP_BAD_REQUEST : Response::HTTP_OK;

        return $this->renderResponse($serviceResponse['content'], $statusCode);
    }
}
