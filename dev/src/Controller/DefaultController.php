<?php declare (strict_types = 1);

namespace ATS\UserBundle\Dev\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ATS\CoreBundle\Controller\Rest\RestControllerTrait;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * DefaultController
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
class DefaultController extends Controller
{
    use RestControllerTrait;

    /**
     * Index action
     *
     * @param Request       $request
     *
     * @return JsonResponse
     */
    public function indexAction(Request $request)
    {
        $data = [
            'foo' => 'bar',
        ];

        return $this->renderResponse($data);
    }
}
