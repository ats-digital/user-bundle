<?php declare(strict_types=1);

namespace ATS\UserBundle\Tests\Unit\Service;

use ATS\UserBundle\Tests\KernelTestCase;

/**
 * ClientServiceTest
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 *
 */
class ClientServiceTest extends KernelTestCase
{
    /**
     * Test suits for get clients
     */
    public function testGetClients()
    {
        $clients = $this->clientService->getClients();

        $this->assertEquals([], $clients);
    }
}
