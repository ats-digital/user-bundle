<?php declare(strict_types=1);

namespace ATS\UserBundle\Tests\Unit\Service;

use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\HttpFoundation\Response;
use ATS\UserBundle\Tests\KernelTestCase;

/**
 * UserServiceTest
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 *
 */
class UserServiceTest extends KernelTestCase
{
    /**
     * Test suits for create with no inputs
     */
    public function testCreateWithoutInputs()
    {
        $userData = json_encode([]);

        $this->expectException(UnexpectedTypeException::class);
        $userServiceResponse = $this->userService->create($userData);
    }

    /**
     * Test suits for create with only username
     */
    public function testCreateWithOnlyUsername()
    {
        $userData = json_encode([
            'username' => 'username',
        ]);

        $this->expectException(UnexpectedTypeException::class);
        $userServiceResponse = $this->userService->create($userData);
    }

    /**
     * Test suits for create with no password
     */
    public function testCreateWithoutPassword()
    {
        $userData = json_encode([
            'username' => 'username',
            'email' => 'wajih@ats-digital.com',
        ]);

        $this->expectException(UnexpectedTypeException::class);
        $userServiceResponse = $this->userService->create($userData);
    }

    /**
     * Test suits for create with invalid email
     */
    public function testCreateWithInvalidEmail()
    {
        $userData = json_encode([
            'username' => 'username',
            'email' => 'wajih@foo.bar',
            'plain_password' => 'password',
        ]);
        $userServiceResponse = $this->userService->Create($userData);

        $expected = [
            "statusCode" => Response::HTTP_BAD_REQUEST,
            "content" => [
                "email" => "error.validation.invalid",
            ]
        ];

        $this->assertEquals($expected, $userServiceResponse);
    }

    /**
     * Test suits for create with invalid password
     */
    public function testCreateWithInvalidPassword()
    {
        $userData = json_encode([
            'username' => 'username',
            'email' => 'wajih@ats-digital.com',
            'plain_password' => 'pass',
        ]);
        $userServiceResponse = $this->userService->create($userData);

        $expected = [
            "statusCode" => Response::HTTP_BAD_REQUEST,
            "content" => [
                "plainPassword" => "error.validation.password_strength.weak",
            ]
        ];

        $this->assertEquals($expected, $userServiceResponse);
    }

    /**
     * Test suits for create success
     */
    public function testCreateSuccess()
    {
        $userData = json_encode([
            'username' => 'username',
            'email' => 'wajih@ats-digital.com',
            'plain_password' => 'password',
        ]);
        $userServiceResponse = $this->userService->create($userData);

        $expected = [
            "statusCode" => Response::HTTP_OK,
            "content" => [
                "user" => "success.action.update",
            ]
        ];

        $this->assertEquals($expected['statusCode'], $userServiceResponse['statusCode']);
        $this->assertEquals($expected['content']['user'], $userServiceResponse['content']['user']);
    }

    /**
     * Test suits for create exist username
     */
    public function testCreateExistUsername()
    {
        $this->userManager->create('username', 'username@ats-digital.com', 'password');
        $userData = json_encode([
            'username' => 'username',
            'email' => 'wajih@ats-digital.com',
            'plain_password' => 'password',
        ]);
        $userServiceResponse = $this->userService->create($userData);

        $expected = [
            "statusCode" => Response::HTTP_BAD_REQUEST,
            "content" => [
                "username" => "error.validation.exist",
            ]
        ];

        $this->assertEquals($expected, $userServiceResponse);
    }

    /**
     * Test suits for create exist email
     */
    public function testCreateExistEmail()
    {
        $this->userManager->create('username', 'username@ats-digital.com', 'password');
        $userData = json_encode([
            'username' => 'foo',
            'email' => 'username@ats-digital.com',
            'plain_password' => 'password',
        ]);
        $userServiceResponse = $this->userService->create($userData);

        $expected = [
            "statusCode" => Response::HTTP_BAD_REQUEST,
            "content" => [
                "email" => "error.validation.exist",
            ]
        ];

        $this->assertEquals($expected, $userServiceResponse);
    }
}
