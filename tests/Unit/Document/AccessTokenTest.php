<?php declare(strict_types=1);

namespace ATS\UserBundle\Tests\Unit\Document;

use ATS\UserBundle\Document\AccessToken;
use ATS\UserBundle\Document\Client;
use ATS\UserBundle\Document\User;
use PHPUnit\Framework\TestCase;

/**
 * AccessTokenTest
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
class AccessTokenTest extends TestCase
{
    /**
     * Test suits for client
     */
    public function testSettterGetterClient()
    {
        $client = new Client();
        $accessToken = new AccessToken();
        $accessToken->setClient($client);
        $this->assertInstanceOf(Client::class, $accessToken->getClient());
    }

    /**
     * Test suits for user
     */
    public function testSettterGetterUser()
    {
        $user = new User();
        $accessToken = new AccessToken();
        $accessToken->setUser($user);
        $this->assertInstanceOf(User::class, $accessToken->getUser());
    }
}
