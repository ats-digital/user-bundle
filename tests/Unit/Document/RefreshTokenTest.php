<?php declare(strict_types=1);

namespace ATS\UserBundle\Tests\Unit\Document;

use ATS\UserBundle\Document\RefreshToken;
use ATS\UserBundle\Document\Client;
use ATS\UserBundle\Document\User;
use PHPUnit\Framework\TestCase;

/**
 * RefreshTokenTest
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
class RefreshTokenTest extends TestCase
{
    /**
     * Test suits for client
     */
    public function testSettterGetterClient()
    {
        $client = new Client();
        $refreshToken = new RefreshToken();
        $refreshToken->setClient($client);
        $this->assertInstanceOf(Client::class, $refreshToken->getClient());
    }

    /**
     * Test suits for user
     */
    public function testSettterGetterUser()
    {
        $user = new User();
        $refreshToken = new RefreshToken();
        $refreshToken->setUser($user);
        $this->assertInstanceOf(User::class, $refreshToken->getUser());
    }
}
