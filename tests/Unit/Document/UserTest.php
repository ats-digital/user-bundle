<?php declare(strict_types=1);

namespace ATS\UserBundle\Tests\Unit\Document;

use ATS\UserBundle\Document\User;
use PHPUnit\Framework\TestCase;

/**
 * UserTest
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
class UserTest extends TestCase
{
    /**
     * Test suits for constructor
     */
    public function testConstructor()
    {
        $user = new User();
        $this->assertEquals(is_array($user->getRoles()), true);
        $this->assertEquals($user->getRoles(), [User::DEFAULT_ROLE]);
        $this->assertEquals($user->hasRole(User::DEFAULT_ROLE), true);
    }

    /**
     * Test suits for username
     */
    public function testSettterGetterUsername()
    {
        $user = new User();
        $user->setUsername('username');
        $this->assertEquals($user->getUsername(), 'username');
    }

    /**
     * Test suits for email
     */
    public function testSettterGetterEmail()
    {
        $user = new User();
        $user->setEmail('username@ats-digital.com');
        $this->assertEquals($user->getEmail(), 'username@ats-digital.com');
    }

    /**
     * Test suits for password
     */
    public function testSettterGetterPassword()
    {
        $user = new User();
        $user->setPassword('hash-password');
        $this->assertEquals($user->getPassword(), 'hash-password');
    }

    /**
     * Test suits for salt
     */
    public function testSettterGetterSalt()
    {
        $user = new User();
        $user->setSalt('salt');
        $this->assertEquals($user->getSalt(), 'salt');
    }

    /**
     * Test suits for activate
     */
    public function testActivate()
    {
        $user = new User();
        $user->activate();
        $this->assertEquals($user->getActive(), true);
    }

    /**
     * Test suits for deactivate
     */
    public function testDectivate()
    {
        $user = new User();
        $user->deactivate();
        $this->assertEquals($user->getActive(), false);
    }

    /**
     * Test suits for isEnabled
     */
    public function testIsEnabled()
    {
        $user = new User();
        $user->activate();
        $this->assertEquals($user->isEnabled(), true);
        $user->deactivate();
        $this->assertEquals($user->isEnabled(), false);
    }

    /**
     * Test suits for expireAt
     */
    public function testSetterGetterExpireAt()
    {
        $user = new User();
        $date = new \DateTime('now', new \DateTimeZone('etc/utc'));

        $user->setExpireAt($date);
        $this->assertInstanceOf(\DateTime::class, $user->getExpireAt());
        $this->assertEquals($user->getExpireAt(), $date);
    }

    /**
     * Test suits for roles
     */
    public function testSetterGetterRoles()
    {
        $user = new User();

        $user->setRoles([]);
        $this->assertEquals(is_array($user->getRoles()), true);
        $this->assertEquals($user->getRoles(), [User::DEFAULT_ROLE]);

        $user->setRoles([User::DEFAULT_ROLE]);
        $this->assertEquals(is_array($user->getRoles()), true);
        $this->assertEquals($user->getRoles(), [User::DEFAULT_ROLE]);

        $user->setRoles(['ROLE_ADMIN']);
        $this->assertEquals(is_array($user->getRoles()), true);
        $this->assertEquals($user->getRoles(), [User::DEFAULT_ROLE, 'ROLE_ADMIN']);

        $user->setRoles(['ROLE_ADMIN', 'ROLE_SUPER_ADMIN']);
        $this->assertEquals(is_array($user->getRoles()), true);
        $this->assertEquals($user->getRoles(), [User::DEFAULT_ROLE, 'ROLE_ADMIN', 'ROLE_SUPER_ADMIN']);
    }

    /**
     * Test suits for roles
     */
    public function testHasRoles()
    {
        $user = new User();

        $user->setRoles(['ROLE_ADMIN']);
        $this->assertEquals(is_array($user->getRoles()), true);
        $this->assertEquals($user->hasRole(User::DEFAULT_ROLE), true);
        $this->assertEquals($user->hasRole('ROLE_ADMIN'), true);
    }

    /**
     * Test suits for roles
     */
    public function testPromote()
    {
        $user = new User();

        $user->setRoles(['ROLE_ADMIN']);
        $user = $user->promote('ROLE_SUPER_ADMIN');
        $this->assertEquals($user->hasRole(User::DEFAULT_ROLE), true);
        $this->assertEquals($user->hasRole('ROLE_ADMIN'), true);
        $this->assertEquals($user->hasRole('ROLE_SUPER_ADMIN'), true);
        $user = $user->promote(User::DEFAULT_ROLE);
        $this->assertEquals($user->getRoles(), [User::DEFAULT_ROLE, 'ROLE_ADMIN', 'ROLE_SUPER_ADMIN']);
    }

    /**
     * Test suits for roles
     */
    public function testDemote()
    {
        $user = new User();

        $this->assertEquals($user->hasRole(User::DEFAULT_ROLE), true);
        $user = $user->demote('ROLE_USER');
        $this->assertEquals($user->hasRole(User::DEFAULT_ROLE), true);

        $user->setRoles(['ROLE_ADMIN']);
        $this->assertEquals($user->hasRole('ROLE_ADMIN'), true);

        $user->demote('ROLE_ADMIN');
        $this->assertEquals($user->hasRole('ROLE_ADMIN'), false);
    }

    /**
     * Test suits for eraseCredentials
     */
    public function testEraseCredentials()
    {
        $user = new User();
        $user->eraseCredentials();
        $this->assertEquals($user->getRoles(), [User::DEFAULT_ROLE]);
    }

    /**
     * Test suits for isAccountNonExpired
     */
    public function testIsAccountNonExpired()
    {
        $user = new User();
        $this->assertEquals($user->isAccountNonExpired(), true);

        $date = new \DateTime('now', new \DateTimeZone('etc/utc'));
        $date->modify('+10 days');

        $user->setExpireAt($date);
        $this->assertEquals($user->isAccountNonExpired(), true);

        $date->modify('-20 days');
        $this->assertEquals($user->isAccountNonExpired(), false);
    }

    /**
     * Test suits for isAccountNonLocked
     */
    public function testIsAccountNonLocked()
    {
        $user = new User();
        $this->assertEquals($user->isAccountNonLocked(), true);
    }

    /**
     * Test suits for isCredentialsNonExpired
     */
    public function testIsCredentialsNonExpired()
    {
        $user = new User();
        $this->assertEquals($user->isCredentialsNonExpired(), true);
    }

    /**
     * Test suits for serialize
     */
    public function testSerialize()
    {
        $user = new User();
        $user->setUsername('username');
        $user->setPassword('password');
        $user->setSalt('salt');
        $user->activate();
        $this->assertEquals(
            $user->serialize(),
            'a:5:{i:0;N;i:1;s:8:"username";i:2;s:8:"password";i:3;s:4:"salt";i:4;b:1;}'
        );
    }

    /**
     * Test suits for unserialize
     */
    public function testUnserialize()
    {
        $user = new User();
        $user->unserialize('a:5:{i:0;N;i:1;s:8:"username";i:2;s:8:"password";i:3;s:4:"salt";i:4;b:1;}');

        $this->assertInstanceOf(User::class, $user);
        $this->assertEquals($user->getId(), null);
        $this->assertEquals($user->getUsername(), 'username');
        $this->assertEquals($user->getPassword(), 'password');
        $this->assertEquals($user->getSalt(), 'salt');
        $this->assertEquals($user->getRoles(), [User::DEFAULT_ROLE]);
        $this->assertEquals($user->getExpireAt(), null);
        $this->assertEquals($user->isAccountNonExpired(), true);
    }

    /**
     * Test suits for isAccountExpired
     */
    public function testIsAccountExpired()
    {
        $user = new User();
        $this->assertEquals($this->invokeMethod($user, 'isAccountExpired'), false);

        $date = new \DateTime('now', new \DateTimeZone('etc/utc'));
        $date->modify('+10 days');

        $user->setExpireAt($date);
        $this->assertEquals($this->invokeMethod($user, 'isAccountExpired'), false);

        $date->modify('-20 days');
        $this->assertEquals($this->invokeMethod($user, 'isAccountExpired'), true);
    }

    /**
     * Test suits for isAccountLocked
     */
    public function testIsAccountLocked()
    {
        $user = new User();
        $this->assertEquals($this->invokeMethod($user, 'isAccountLocked'), false);
    }

    /**
     * Test suits for isCredentialsExpired
     */
    public function testIsCredentialsExpired()
    {
        $user = new User();
        $this->assertEquals($this->invokeMethod($user, 'isCredentialsExpired'), false);
    }

    /**
     * Invoke method to test private methods by reflection
     *
     * @param User   $object
     * @param string $methodName
     * @param array  $parameters
     *
     * @return mixed
     */
    private function invokeMethod(User &$object, $methodName, array $parameters = [])
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }
}
