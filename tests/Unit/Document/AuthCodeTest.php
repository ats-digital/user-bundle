<?php declare(strict_types=1);

namespace ATS\UserBundle\Tests\Unit\Document;

use ATS\UserBundle\Document\AuthCode;
use ATS\UserBundle\Document\Client;
use ATS\UserBundle\Document\User;
use PHPUnit\Framework\TestCase;

/**
 * AuthCodeTest
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
class AuthCodeTest extends TestCase
{
    /**
     * Test suits for client
     */
    public function testSettterGetterClient()
    {
        $client = new Client();
        $authCode = new AuthCode();
        $authCode->setClient($client);
        $this->assertInstanceOf(Client::class, $authCode->getClient());
    }

    /**
     * Test suits for user
     */
    public function testSettterGetterUser()
    {
        $user = new User();
        $authCode = new AuthCode();
        $authCode->setUser($user);
        $this->assertInstanceOf(User::class, $authCode->getUser());
    }
}
