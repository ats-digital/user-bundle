<?php declare(strict_types=1);

namespace ATS\UserBundle\Tests\Unit\DependencyIntection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use ATS\UserBundle\DependencyInjection\Configuration;
use PHPUnit\Framework\TestCase;

/**
 * ConfigurationTest
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
class ConfigurationTest extends TestCase
{
    /**
     * Test suits for getConfigTreeBuilder
     */
    public function testGgetConfigTreeBuilder()
    {
        $configuration = new Configuration();
        $this->assertInstanceOf(TreeBuilder::class, $configuration->getConfigTreeBuilder());
    }
}
