<?php declare(strict_types=1);

namespace ATS\UserBundle\Tests\Unit\Util;

use ATS\UserBundle\Tests\KernelTestCase;
use ATS\UserBundle\Validator\Constraints\PasswordStrengthValidator;
use ATS\UserBundle\Util\PasswordUtils;

/**
 * PasswordUtilsTest
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 *
 */
class PasswordUtilsTest extends KernelTestCase
{
    /**
     * Test suits for generate random weak
     */
    public function testGenerateRandomWeak()
    {
        $passwordUtils = new PasswordUtils();

        // Weak
        $password = $passwordUtils->generateRandom(PasswordStrengthValidator::WEAK_STRENGTH);
        $this->assertEquals(PasswordStrengthValidator::MINIMUM_LENGTH, strlen($password));
        $pattern = '/^\S*(?=\S{' . PasswordStrengthValidator::MINIMUM_LENGTH . ',})\S*$/';
        preg_match($pattern, $password, $matches);
        $this->assertEquals(1, count($matches));
    }

    /**
     * Test suits for generate random low
     */
    public function testGenerateRandomLow()
    {
        $passwordUtils = new PasswordUtils();

        // low
        $password = $passwordUtils->generateRandom(PasswordStrengthValidator::LOW_STRENGTH);
        $this->assertEquals(PasswordStrengthValidator::MINIMUM_LENGTH, strlen($password));
        $pattern = '/^\S*(?=\S{' . PasswordStrengthValidator::MINIMUM_LENGTH . ',})(?=\S*[A-Z])\S*$/';
        preg_match($pattern, $password, $matches);
        $this->assertEquals(1, count($matches));
    }

    /**
     * Test suits for generate random medium
     */
    public function testGenerateRandomMedium()
    {
        $passwordUtils = new PasswordUtils();

        // Medium
        $password = $passwordUtils->generateRandom(PasswordStrengthValidator::MEDIUM_STRENGTH);
        $this->assertEquals(PasswordStrengthValidator::MINIMUM_LENGTH, strlen($password));
        $pattern = '/^\S*(?=\S{' . PasswordStrengthValidator::MINIMUM_LENGTH . ',})(?=\S*[a-z]?)(?=\S*[A-Z])(?=\S*[\d])(?=\S*[\d])(?=\S*[\&\@\?\!\(){}\/\+\-*~\#_]?)\S*$/';
        preg_match($pattern, $password, $matches);
        $this->assertEquals(1, count($matches));
    }

    /**
     * Test suits for generate random strong
     */
    public function testGenerateRandomStrong()
    {
        $passwordUtils = new PasswordUtils();

        // Strong
        $password = $passwordUtils->generateRandom(PasswordStrengthValidator::STRONG_STRENGTH);
        $this->assertEquals(PasswordStrengthValidator::MINIMUM_LENGTH, strlen($password));
        $pattern = '/^\S*(?=\S{' . PasswordStrengthValidator::MINIMUM_LENGTH . ',})(?=\S*[a-z]?)(?=\S*[A-Z])(?=\S*[\d])(?=\S*[\&\@\?\!\(){}\/\+\-*~\#_])\S*$/';
        preg_match($pattern, $password, $matches);
        $this->assertEquals(1, count($matches));
    }

    /**
     * Test suits for generate random not in range
     */
    public function testGenerateRandomNotInRange()
    {
        $passwordUtils = new PasswordUtils();

        // Something else
        $password = $passwordUtils->generateRandom('foo');
        $this->assertEquals(PasswordStrengthValidator::MINIMUM_LENGTH, strlen($password));
        $pattern = '/^\S*(?=\S{' . PasswordStrengthValidator::MINIMUM_LENGTH . ',})\S*$/';
        preg_match($pattern, $password, $matches);
        $this->assertEquals(1, count($matches));
    }

    /**
     * Test suits for generate random low length
     */
    public function testGenerateRandomLowLength()
    {
        $passwordUtils = new PasswordUtils();

        // length < minimum authorized
        $password = $passwordUtils->generateRandom('foo', 2);
        $this->assertEquals(PasswordStrengthValidator::MINIMUM_LENGTH, strlen($password));
    }

    /**
     * Test suits for generate random higher length
     */
    public function testGenerateRandomHigherLength()
    {
        $passwordUtils = new PasswordUtils();

        // length > default
        $password = $passwordUtils->generateRandom('foo', 8);
        $this->assertEquals(8, strlen($password));
    }
}
