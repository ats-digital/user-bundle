<?php declare(strict_types=1);

namespace ATS\UserBundle\Tests\Unit\Util;

use ATS\UserBundle\Tests\KernelTestCase;
use ATS\UserBundle\Validator\Constraints\PasswordStrength as PasswordStrengthConstraint;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
/**
 * PasswordStrengthValidatorTest
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 *
 */
class PasswordStrengthValidatorTest extends KernelTestCase
{
    /**
     * Test suits for validate
     */
    public function testValidateNullPassword()
    {
        $constraint = new PasswordStrengthConstraint();
        // Null
        $this->expectException(UnexpectedTypeException::class);
        $errors = $this->validator->validate(
            null,
            $constraint
        );
    }

    /**
     * Test suits for validate
     */
    public function testValidate()
    {
        $constraint = new PasswordStrengthConstraint();

        // Weak invalid
        $errors = $this->validator->validate(
            'pass',
            $constraint
        );
        $this->assertEquals('error.validation.password_strength.weak', $errors[0]->getMessage());

        // Weak valid
        $errors = $this->validator->validate(
            'password',
            $constraint
        );
        $this->assertEquals(0, count($errors));

        // Exception
        $this->expectException(UnexpectedTypeException::class);
        $errors = $this->validator->validate(
            [],
            $constraint
        );
    }
}
