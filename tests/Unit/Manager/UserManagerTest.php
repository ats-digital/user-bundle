<?php declare(strict_types=1);

namespace ATS\UserBundle\Tests\Unit\Manager;

use ATS\UserBundle\Tests\KernelTestCase;
use ATS\UserBundle\Document\User;

/**
 * UserManagerTest
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 *
 */
class UserManagerTest extends KernelTestCase
{
    /**
     * Test suits for create & load
     */
    public function testCreateLoadUser()
    {
        $user = $this->userManager->create(
            'username',
            'username@ats-digital.com',
            'password',
            true
        );
        $this->assertEquals($user->isEnabled(), true);
        $this->assertInstanceOf(User::class, $this->userManager->loadUserByUsernameOrEmail('username'));
    }

    /**
     * Test suits for activate & deactivate
     */
    public function testActivateDeactivateUser()
    {
        $user = $this->userManager->create(
            'username',
            'username@ats-digital.com',
            'password',
            true
        );
        $this->assertInstanceOf(User::class, $user);
        $this->assertEquals($user->isEnabled(), true);
        $this->userManager->deactivate($user);
        $this->assertEquals($user->isEnabled(), false);
        $this->userManager->activate($user);
        $this->assertEquals($user->isEnabled(), true);
    }

    /**
     *Test suits for change-password
     */
    public function testChangePassword()
    {
        $user = $this->userManager->create(
            'username',
            'username@ats-digital.com',
            'password'
        );

        $newPassword = 'new-password';
        $usr = $this->userManager->changePassword('username', $newPassword);
        $this->userManager->update($usr);
        $encoder = $this->container->get('security.password_encoder');
        $encodedNewPassword = $encoder->encodePassword($user, $newPassword);
        $user = $this->userManager->loadUserByUsernameOrEmail('username');
        $this->assertEquals($encodedNewPassword, $user->getPassword());
    }

    /**
     *Test suits for promote-user
     */
    public function testPromote()
    {
        $user = $this->userManager->create("username", "username@ats-digital.com", "password");

        $newRoleUnknown = "ROLE_TEST";
        $user = $this->userManager->promote("username",$newRoleUnknown);
        $this->assertNull($user);
        $user = $this->userManager->loadUserByUsernameOrEmail("username");
        $this->assertEquals($user->hasRole($newRoleUnknown), false);

        $newRole = "ROLE_ADMIN";
        $this->userManager->promote("username",$newRole);
        $this->assertInstanceOf(User::class, $user);
        $user = $this->userManager->loadUserByUsernameOrEmail("username");
        $this->assertEquals($user->hasRole($newRole), true);
    }

    /**
     *Test suits for demote-user
     */
    public function testDemote()
    {
        $user = $this->userManager->create("username", "username@ats-digital.com", "password");

        $newRoleUnknown = "ROLE_TEST";
        $user = $this->userManager->demote("username",$newRoleUnknown);
        $this->assertNull($user);
        $user = $this->userManager->loadUserByUsernameOrEmail("username");
        $this->assertEquals($user->hasRole($newRoleUnknown), false);

        $newRole = "ROLE_ADMIN";
        $this->userManager->demote("username",$newRole);
        $this->assertInstanceOf(User::class, $user);
        $user = $this->userManager->loadUserByUsernameOrEmail("username");
        $this->assertEquals($user->hasRole($newRole), false);
    }

    /**
     * Test suits for bach update
     */
    public function testBatchUpdate()
    {
        $user1 = new User();
        $user1->setUsername('username1');
        $user1->setEmail('username1@ats-digital.com');
        $user1->setPlainPassword('password1');

        $user2 = new User();
        $user2->setUsername('username2');
        $user2->setEmail('username2@ats-digital.com');
        $user2->setPlainPassword('password2');
        $user2->setActive(true);

        $this->userManager->batchUpdate([$user1, $user2]);
        $users = $this->documentManager->getRepository(User::class)->getAll();

        $this->assertEquals(2, count($users));
        foreach ($users as $user) {
            $this->assertInstanceOf(User::class, $user);
            $this->assertNull($user->getPlainPassword());
            $this->assertNotNull($user->getPassword());
        }
        $this->assertEquals($this->userManager->loadUserByUsernameOrEmail('username1')->isEnabled(), false);
        $this->assertEquals($this->userManager->loadUserByUsernameOrEmail('username2')->isEnabled(), true);
    }
}
