<?php declare(strict_types=1);

namespace ATS\UserBundle\Tests\Unit\Provider;

use ATS\UserBundle\Tests\KernelTestCase;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\User as SfUser;
use ATS\UserBundle\Document\User;

/**
 * UserProviderTest
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
class UserProviderTest extends KernelTestCase
{
    /**
     * Test suits for loadUserByUsername
     */
    public function testLoadUserByUsername()
    {
        $userProvider = $this->container->get('ats_user.provider.user_provider');
        $user = $this->userManager->create(
            'username',
            'username@ats-digital.com',
            'password'
        );
        $userLoaded = $userProvider->loadUserByUsername('username');

        $this->assertInstanceOf(User::class, $userLoaded);
        $this->assertEquals($userLoaded->getUsername(), $user->getUsername());
        $this->assertEquals($userLoaded->getEmail(), $user->getEmail());
    }

    /**
     * Test suits for refreshuser
     */
    public function testRefreshUser()
    {
        $userProvider = $this->container->get('ats_user.provider.user_provider');
        $user = $this->userManager->create(
            'username',
            'username@ats-digital.com',
            'password'
        );
        $userLoaded = $userProvider->loadUserByUsername('username');
        $userRefreshed = $userProvider->refreshUser($userLoaded);

        $this->assertInstanceOf(User::class, $userRefreshed);
        $this->assertEquals($userRefreshed->getUsername(), $user->getUsername());
        $this->assertEquals($userRefreshed->getEmail(), $user->getEmail());

        $sfUser = new SfUser('sf-user', 'sf-user-password');
        $this->expectException(UnsupportedUserException::class);
        $userRefreshed = $userProvider->refreshUser($sfUser);
    }

    /**
     * Test suits for fetchUser
     */
    public function testFetchUser()
    {
        $userProvider = $this->container->get('ats_user.provider.user_provider');
        $user = $this->userManager->create(
            'username',
            'username@ats-digital.com',
            'password'
        );
        $userFetched = $this->invokeMethod($userProvider, 'fetchUser', ['username']);

        $this->assertInstanceOf(User::class, $userFetched);
        $this->assertEquals($userFetched->getUsername(), $user->getUsername());
        $this->assertEquals($userFetched->getEmail(), $user->getEmail());

        $this->expectException(UsernameNotFoundException::class);
        $userFetched = $this->invokeMethod($userProvider, 'fetchUser', ['test-user-3']);
    }
}
