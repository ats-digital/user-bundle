<?php declare(strict_types=1);

namespace ATS\UserBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase as BaseKernelTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Doctrine\ODM\MongoDB\DocumentManager;
use ATS\UserBundle\Manager\UserManager;
use ATS\UserBundle\Service\UserService;
use JMS\serializer\Serializer;

/**
 * KernelTestCase
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
abstract class KernelTestCase extends BaseKernelTestCase
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var DocumentManager
     */
    protected $documentManager;

    /**
     * @var UserManager
     */
    protected $userManager;

    /**
     * @var UserService
     */
    protected $userService;

    /**
     * @var ClientManager
     */
    protected $clientManager;

    /**
     * @var ClientService
     */
    protected $clientService;

    /**
     * @var string
     */
    protected $userClass;

    /**
     * @var Serializer
     */
    protected $serializer;

    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        self::bootKernel();
        $this->container = self::$kernel->getContainer();
        $this->documentManager = $this->container->get('doctrine_mongodb.odm.document_manager');
        $this->userManager = $this->container->get('ats_user.manager.user_manager');
        $this->userService = $this->container->get('ats_user.service.user_service');
        $this->clientManager = $this->container->get('ats_user.manager.client_manager');
        $this->clientService = $this->container->get('ats_user.service.client_service');
        $this->userClass = $this->container->getParameter('ats_user.user_class');
        $this->serializer = $this->container->get('jms_serializer');
        $this->validator = $this->container->get('validator');

        $this->documentManager->getSchemaManager()->dropDatabases();
        $this->documentManager->getSchemaManager()->updateIndexes();
    }

    /**
     * {@inheritDoc}
     */
    public function tearDown()
    {
        $this->documentManager->getSchemaManager()->dropDatabases();
    }

    /**
     * Invoke method to test protected methods by reflection
     *
     * @param mixed  $object
     * @param string $methodName
     * @param array  $parameters
     *
     * @return mixed
     */
    protected function invokeMethod(&$object, $methodName, array $parameters = [])
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }
}
