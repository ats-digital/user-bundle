<?php declare(strict_types=1);

namespace ATS\UserBundle\Tests\Functional\Command;

use ATS\UserBundle\Tests\WebTestCase;
use Symfony\Component\Console\Tester\CommandTester;
use ATS\UserBundle\Command\EnableUserCommand;

/**
 * EnableUserCommandTest
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
class EnableUserCommandTest extends WebTestCase
{
    /**
     * Test suits for enable user command
     */
    public function testExecute()
    {
        $cmd = new EnableUserCommand($this->logger);
        $this->application->add($cmd);
        $command = $this->application->find('ats:user:enable');

        $user = $this->userManager->create(
            'username',
            'username@ats-digital.com',
            'password'
        );

        $this->assertEquals($user->isEnabled(), false);
        $this->documentManager->clear();

        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command'  => $command->getName(),
            'username' => 'username',
        ]);

        $output = $commandTester->getDisplay();
        $this->assertContains('User username enabled', $output);
        $user = $this->userManager->loadUserByUsernameOrEmail('username');
        $this->assertEquals($user->isEnabled(), true);
    }

    /**
     * Test suits for enable user command with username
     */
    public function testExecuteWithUsername()
    {
        $cmd = new EnableUserCommand($this->logger);
        $this->application->add($cmd);
        $command = $this->application->find('ats:user:enable');

        $user = $this->userManager->create(
            'username',
            'username@ats-digital.com',
            'password'
        );

        $this->assertEquals($user->isEnabled(), false);
        $this->documentManager->clear();

        $commandTester = new CommandTester($command);
        $commandTester->setInputs([
            'username' => 'username',
        ]);
        $commandTester->execute(['command'  => $command->getName()]);

        $output = $commandTester->getDisplay();
        $this->assertContains('User username enabled', $output);
        $user = $this->userManager->loadUserByUsernameOrEmail('username');
        $this->assertEquals($user->isEnabled(), true);
    }

    /**
     * Test suits for enable user command without username
     */
    public function testExecuteWithoutUsername()
    {
        $cmd = new EnableUserCommand($this->logger);
        $this->application->add($cmd);
        $command = $this->application->find('ats:user:enable');

        $commandTester = new CommandTester($command);
        $commandTester->setInputs([
            'username' => ' ',
        ]);

        $this->expectException(\Exception::class);
        $commandTester->execute(['command'  => $command->getName()]);
    }

    /**
     * Test suits for enable user command user not exist
     */
    public function testExecuteUserNotExist()
    {
        $cmd = new EnableUserCommand($this->logger);
        $this->application->add($cmd);
        $command = $this->application->find('ats:user:enable');

        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command'  => $command->getName(),
            'username' => 'username',
        ]);

        $output = $commandTester->getDisplay();
        $this->assertContains('User with username username does not exist', $output);
    }
}
