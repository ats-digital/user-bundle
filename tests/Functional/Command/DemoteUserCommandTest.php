<?php declare(strict_types=1);

namespace ATS\UserBundle\Tests\Functional\Command;

use ATS\UserBundle\Tests\WebTestCase;
use Symfony\Component\Console\Tester\CommandTester;
use ATS\UserBundle\Command\DemoteUserCommand;

/**
 *DemoteUserCommandTest
 *
 * @author
 */
class DemoteUserCommandTest extends WebTestCase
{
    /**
     * Test suits for demote user command
     */
    public function testExecute()
    {
        $cmd = new DemoteUserCommand($this->logger);
        $this->application->add($cmd);
        $command = $this->application->find('ats:user:demote');
        $this->userManager->create('username', 'username@ats-digital.com', 'password');

        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command'  => $command->getName(),
            'username' => 'username',
            'role' => 'ROLE_ADMIN',
        ]);

        $output = $commandTester->getDisplay();
        $this->assertContains('User username demoted with role ROLE_ADMIN', $output);
    }

    /**
     * Test suits for demote user command user not exist
     */
    public function testExecuteUserNotExist()
    {
        $cmd = new DemoteUserCommand($this->logger);
        $this->application->add($cmd);
        $command = $this->application->find('ats:user:demote');

        $commandTester = new CommandTester($command);
        $commandTester->setInputs([
            'username' => 'username',
            'role' => 'ROLE_ADMIN',
        ]);

        $commandTester->execute(['command'  => $command->getName()]);
        $output = $commandTester->getDisplay();
        $this->assertContains('User username does not exits or role ROLE_ADMIN is not supported', $output);
    }

    /**
     * Test suits for demote user command role not exist
     */
    public function testExecuteRoleNotExist()
    {
        $cmd = new DemoteUserCommand($this->logger);
        $this->application->add($cmd);
        $command = $this->application->find('ats:user:demote');
        $this->userManager->create('username', 'username@ats-digital.com', 'password');

        $commandTester = new CommandTester($command);
        $commandTester->setInputs([
            'username' => 'username',
            'role' => 'ROLE_FOO',
        ]);

        $this->expectException(\Exception::class);
        $commandTester->execute(['command'  => $command->getName()]);
    }

    /**
     * Test suits for demote user command without username
     */
    public function testExecuteWithoutUsername()
    {
        $cmd = new DemoteUserCommand($this->logger);
        $this->application->add($cmd);
        $command = $this->application->find('ats:user:demote');

        $commandTester = new CommandTester($command);
        $commandTester->setInputs([
            'username' => null,
            'role' => 'ROLE_ADMIN',
        ]);

        $this->expectException(\Exception::class);
        $commandTester->execute(['command'  => $command->getName()]);
    }

    /**
     * Test suits for demote user command without role
     */
    public function testExecuteWithoutRole()
    {
        $cmd = new DemoteUserCommand($this->logger);
        $this->application->add($cmd);
        $command = $this->application->find('ats:user:demote');

        $commandTester = new CommandTester($command);
        $commandTester->setInputs([
            'username' => 'username',
            'role' => ' ',
        ]);

        $this->expectException(\Exception::class);
        $commandTester->execute(['command'  => $command->getName()]);
    }
}
