<?php declare(strict_types=1);

namespace ATS\UserBundle\Tests\Functional\Command;

use ATS\UserBundle\Tests\WebTestCase;
use Symfony\Component\Console\Tester\CommandTester;
use ATS\UserBundle\Command\ChangePasswordCommand;

/**
 * ChangePasswordCommandTest
 *
 * @author Elyes MANAI <emanai@ats-digital.com>
 */
class ChangePasswordCommandTest extends WebTestCase
{
    /**
     * Test suits for change password command
     */
    public function testExecute()
    {
        $cmd = new ChangePasswordCommand($this->logger, $this->passwordUtils, $this->validator);
        $this->application->add($cmd);
        $command = $this->application->find('ats:user:change-password');

        $this->userManager->create(
            'username',
            'username@ats-digital.com',
            'password'
        );

        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command'  => $command->getName(),
            'username' => 'username',
            'password' => 'new-password',
        ]);

        $output = $commandTester->getDisplay();
        $this->assertContains('Change password for user username succeeded', $output);
    }

    /**
     * Test suits for change password command user not exist
     */
    public function testExecuteUserNotExist()
    {
        $cmd = new ChangePasswordCommand($this->logger, $this->passwordUtils, $this->validator);
        $this->application->add($cmd);
        $command = $this->application->find('ats:user:change-password');

        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command'  => $command->getName(),
            'username' => 'username',
            'password' => 'password',
        ]);

        $output = $commandTester->getDisplay();
        $this->assertContains('User with username username does not exist', $output);
    }

    /**
     * Test suits for change password command without username
     */
    public function testExecuteWithoutUsername()
    {
        $cmd = new ChangePasswordCommand($this->logger, $this->passwordUtils, $this->validator);
        $this->application->add($cmd);
        $command = $this->application->find('ats:user:change-password');

        $commandTester = new CommandTester($command);
        $commandTester->setInputs([
            'username' => null,
            'password' => 'password',
        ]);

        $this->expectException(\Exception::class);
        $commandTester->execute(['command'  => $command->getName()]);
    }

    /**
     * Test suits for change password command without password
     */
    public function testExecuteWithoutPassword()
    {
        $cmd = new ChangePasswordCommand($this->logger, $this->passwordUtils, $this->validator);
        $this->application->add($cmd);
        $command = $this->application->find('ats:user:change-password');

        $this->userManager->create(
            'username',
            'username@ats-digital.com',
            'password'
        );

        $commandTester = new CommandTester($command);
        $commandTester->setInputs([
            'username' => 'username',
            'password' => ' ',
        ]);

        $commandTester->execute(['command'  => $command->getName()]);
        $output = $commandTester->getDisplay();
        $this->assertContains('Change password for user username succeeded with new password', $output);
    }

    /**
     * Test suits for create user command with invalid password
     */
    public function testExecuteWithInvalidPassword()
    {
        $cmd = new ChangePasswordCommand($this->logger, $this->passwordUtils, $this->validator);
        $this->application->add($cmd);
        $command = $this->application->find('ats:user:change-password');

        $this->userManager->create(
            'username',
            'username@ats-digital.com',
            'password'
        );

        $commandTester = new CommandTester($command);

        $this->expectException(\Exception::class);
        $commandTester->execute([
            'command'  => $command->getName(),
            'username' => 'username',
            'password' => 'pass',
        ]);
    }

    /**
     * Test suits for create user command with invalid password
     */
    public function testExecuteWithInvalidPasswordInteract()
    {
        $cmd = new ChangePasswordCommand($this->logger, $this->passwordUtils, $this->validator);
        $this->application->add($cmd);
        $command = $this->application->find('ats:user:change-password');

        $commandTester = new CommandTester($command);
        $commandTester->setInputs([
            'username' => 'username',
            'password' => 'pass',
        ]);

        $this->expectException(\Exception::class);
        $commandTester->execute(['command'  => $command->getName()]);
    }
}
