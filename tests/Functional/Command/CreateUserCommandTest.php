<?php declare(strict_types=1);

namespace ATS\UserBundle\Tests\Functional\Command;

use ATS\UserBundle\Tests\WebTestCase;
use Symfony\Component\Config\Definition\Exception\MongoDuplicateKeyException;
use Symfony\Component\Console\Tester\CommandTester;
use ATS\UserBundle\Command\CreateUserCommand;
/**
 * CreateUserCommandTest
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
class CreateUserCommandTest extends WebTestCase
{
    /**
     * Test suits for create user command
     */
    public function testExecute()
    {
        $cmd = new CreateUserCommand($this->logger, $this->passwordUtils, $this->validator);
        $this->application->add($cmd);
        $command = $this->application->find('ats:user:create');

        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command'  => $command->getName(),
            'username' => 'username',
            'email'    => 'username@ats-digital.com',
            'password' => 'password',
        ], [
            'activate' => true,
        ]);

        $output = $commandTester->getDisplay();
        $this->assertContains('Created user username', $output);
    }

    /**
     * Test suits for create user command without username
     */
    public function testExecuteWithoutUsername()
    {
        $cmd = new CreateUserCommand($this->logger, $this->passwordUtils, $this->validator);
        $this->application->add($cmd);
        $command = $this->application->find('ats:user:create');

        $commandTester = new CommandTester($command);
        $commandTester->setInputs([
            'username' => null,
            'email' => 'username@ats-digital.com',
            'password' => 'password',
        ]);

        $this->expectException(\Exception::class);
        $commandTester->execute(['command'  => $command->getName()]);
    }

    /**
     * Test suits for create user command without email
     */
    public function testExecuteWithoutEmail()
    {
        $cmd = new CreateUserCommand($this->logger, $this->passwordUtils, $this->validator);
        $this->application->add($cmd);
        $command = $this->application->find('ats:user:create');

        $commandTester = new CommandTester($command);
        $commandTester->setInputs([
            'username' => 'username',
            'email' => null,
            'password' => 'password',
        ]);

        $this->expectException(\Exception::class);
        $commandTester->execute(['command'  => $command->getName()]);
    }

    /**
     * Test suits for create user command with auto generated password
     */
    public function testExecuteWithPassword()
    {
        $cmd = new CreateUserCommand($this->logger, $this->passwordUtils, $this->validator);
        $this->application->add($cmd);
        $command = $this->application->find('ats:user:create');

        $commandTester = new CommandTester($command);
        $commandTester->setInputs([
            'username' => 'username',
            'email' => 'username@ats-digital.com',
            'password' => ' ',
        ]);

        $commandTester->execute(['command'  => $command->getName()]);
        $output = $commandTester->getDisplay();
        $this->assertContains('Created user username with password', $output);
    }

    /**
     * Test suits for create user command with invalid password
     */
    public function testExecuteWithInvalidPassword()
    {
        $cmd = new CreateUserCommand($this->logger, $this->passwordUtils, $this->validator);
        $this->application->add($cmd);
        $command = $this->application->find('ats:user:create');

        $commandTester = new CommandTester($command);

        $this->expectException(\Exception::class);
        $commandTester->execute([
            'command'  => $command->getName(),
            'username' => 'username',
            'email' => 'username@ats-digital.com',
            'password' => 'pass',
        ]);
    }

    /**
     * Test suits for create user command with invalid password
     */
    public function testExecuteWithInvalidPasswordInteract()
    {
        $cmd = new CreateUserCommand($this->logger, $this->passwordUtils, $this->validator);
        $this->application->add($cmd);
        $command = $this->application->find('ats:user:create');

        $commandTester = new CommandTester($command);
        $commandTester->setInputs([
            'username' => 'username',
            'email' => 'username@ats-digital.com',
            'password' => 'pass',
        ]);

        $this->expectException(\Exception::class);
        $commandTester->execute(['command'  => $command->getName()]);
    }

    /**
     * Test suits for create user command using existing username
     */
    public function testExecuteExistsUser()
    {
        $cmd = new CreateUserCommand($this->logger, $this->passwordUtils, $this->validator);
        $this->application->add($cmd);
        $command = $this->application->find('ats:user:create');

        $this->userManager->create(
            'username',
            'username@ats-digital.com',
            'password'
        );

        $commandTester = new CommandTester($command);
        $commandTester->setInputs([
            'username' => 'username',
            'email' => 'username@ats-digital.com',
            'password' => 'password',
        ]);

        $commandTester->execute(['command'  => $command->getName()]);
        $output = $commandTester->getDisplay();
        $this->assertContains('User with username "username" alerady exists', $output);
    }
}