<?php declare(strict_types=1);

namespace ATS\UserBundle\Tests\Functional\Command;

use ATS\UserBundle\Tests\WebTestCase;
use Symfony\Component\Console\Tester\CommandTester;
use ATS\UserBundle\Command\DisableUserCommand;

/**
 * DisableUserCommandTest
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
class DisableUserCommandTest extends WebTestCase
{
    /**
     * Test suits for disable user command
     */
    public function testExecute()
    {
        $cmd = new DisableUserCommand($this->logger);
        $this->application->add($cmd);
        $command = $this->application->find('ats:user:disable');

        $user = $this->userManager->create(
            'username',
            'username@ats-digital.com',
            'password',
            true
        );

        $this->assertEquals($user->isEnabled(), true);
        $this->documentManager->clear();

        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command'  => $command->getName(),
            'username' => 'username',
        ]);

        $output = $commandTester->getDisplay();
        $this->assertContains('User username disabled', $output);
        $user = $this->userManager->loadUserByUsernameOrEmail('username');
        $this->assertEquals($user->isEnabled(), false);
    }

    /**
     * Test suits for disable user command with username
     */
    public function testExecuteWithUsername()
    {
        $cmd = new DisableUserCommand($this->logger);
        $this->application->add($cmd);
        $command = $this->application->find('ats:user:disable');

        $user = $this->userManager->create(
            'username',
            'username@ats-digital.com',
            'password',
            true
        );

        $this->assertEquals($user->isEnabled(), true);
        $this->documentManager->clear();

        $commandTester = new CommandTester($command);
        $commandTester->setInputs([
            'username' => 'username',
        ]);
        $commandTester->execute(['command'  => $command->getName()]);

        $output = $commandTester->getDisplay();
        $this->assertContains('User username disabled', $output);
        $user = $this->userManager->loadUserByUsernameOrEmail('username');
        $this->assertEquals($user->isEnabled(), false);
    }

    /**
     * Test suits for disable user command without username
     */
    public function testExecuteWithoutUsername()
    {
        $cmd = new DisableUserCommand($this->logger);
        $this->application->add($cmd);
        $command = $this->application->find('ats:user:disable');

        $commandTester = new CommandTester($command);
        $commandTester->setInputs([
            'username' => ' ',
        ]);

        $this->expectException(\Exception::class);
        $commandTester->execute(['command'  => $command->getName()]);
    }

    /**
     * Test suits for disable user command user not exist
     */
    public function testExecuteUserNotExist()
    {
        $cmd = new DisableUserCommand($this->logger);
        $this->application->add($cmd);
        $command = $this->application->find('ats:user:disable');

        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command'  => $command->getName(),
            'username' => 'username',
        ]);

        $output = $commandTester->getDisplay();
        $this->assertContains('User with username username does not exist', $output);
    }
}
