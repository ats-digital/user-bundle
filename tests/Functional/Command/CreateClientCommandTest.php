<?php declare(strict_types=1);

namespace ATS\UserBundle\Tests\Functional\Command;

use ATS\UserBundle\Tests\WebTestCase;
use Symfony\Component\Config\Definition\Exception\MongoDuplicateKeyException;
use Symfony\Component\Console\Tester\CommandTester;
use ATS\UserBundle\Command\CreateUserCommand;
/**
 * CreateClientCommandTest
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
class CreateClientCommandTest extends WebTestCase
{
    /**
     * Test suits for create client command
     */
    public function testExecute()
    {
        $cmd = new CreateUserCommand($this->logger, $this->passwordUtils, $this->validator);
        $this->application->add($cmd);
        $command = $this->application->find('ats:user:client:create');

        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command'  => $command->getName(),
            'name' => 'client'
        ]);

        $output = $commandTester->getDisplay();
        $this->assertContains('Created client', $output);
    }

    /**
     * Test suits for create client command without name
     */
    public function testExecuteWithoutName()
    {
        $cmd = new CreateUserCommand($this->logger, $this->passwordUtils, $this->validator);
        $this->application->add($cmd);
        $command = $this->application->find('ats:user:client:create');

        $commandTester = new CommandTester($command);
        $commandTester->setInputs([
            'name' => null,
        ]);

        $this->expectException(\Exception::class);
        $commandTester->execute(['command'  => $command->getName()]);
    }

    /**
     * Test suits for create client command without name
     */
    public function testExecuteWithoutNameInteract()
    {
        $cmd = new CreateUserCommand($this->logger, $this->passwordUtils, $this->validator);
        $this->application->add($cmd);
        $command = $this->application->find('ats:user:client:create');

        $commandTester = new CommandTester($command);
        $commandTester->setInputs([
            'name' => ' ',
        ]);

        $this->expectException(\Exception::class);
        $commandTester->execute(['command'  => $command->getName()]);
    }

    /**
     * Test suits for create client command using existing name
     */
    public function testExecuteExistsClient()
    {
        $cmd = new CreateUserCommand($this->logger, $this->passwordUtils, $this->validator);
        $this->application->add($cmd);
        $command = $this->application->find('ats:user:client:create');

        $this->clientManager->create('client');

        $commandTester = new CommandTester($command);
        $commandTester->setInputs([
            'name' => 'client',
        ]);

        $commandTester->execute(['command'  => $command->getName()]);
        $output = $commandTester->getDisplay();
        $this->assertContains('Client "client" alerady exists', $output);
    }
}