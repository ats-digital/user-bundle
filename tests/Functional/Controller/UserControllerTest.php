<?php declare(strict_types=1);

namespace ATS\UserBundle\Tests\Functional\Controller;

use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use ATS\UserBundle\Tests\WebTestCase;

/**
 * UserControllerTest
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
class UserControllerTest extends WebTestCase
{
    /**
     * Test suits for registerAction with empty inputs
     */
    public function testRegisterActionWithEmptyInputs()
    {
        $url = $this->container->get('router')->generate('ats_user_user_register');

        $this->expectException(UnexpectedTypeException::class);
        $this->client->request('POST', $url);
    }

    /**
     * Test suits for registerAction
     */
    public function testRegisterAction()
    {
        $url = $this->container->get('router')->generate('ats_user_user_register');

        $userData = [
            'username' => 'username',
            'email' => 'username@ats-digital.com',
            'plain_password' => 'password',
        ];

        $this->client->request('POST', $url, [], [], [], json_encode($userData));
        $response = $this->client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertInstanceOf(JsonResponse::class, $response);

        $expected = [
            'user' => 'success.action.update',
        ];
        $this->assertEquals($expected['user'], json_decode($response->getContent(), true)['user']);
    }

    /**
     * Test suits for resetPasswordAction with empty inputs
     */
    public function testResetPasswordActionWithEmptyInputs()
    {
        $url = $this->container->get('router')->generate('ats_user_user_reset_password');

        $this->client->request('POST', $url);
        $response = $this->client->getResponse();

        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());
        $this->assertInstanceOf(JsonResponse::class, $response);

        $expected = [
            'response' => 'reset_password.missing.username',
        ];
        $this->assertEquals($expected, json_decode($response->getContent(), true));
    }

    /**
     * Test suits for resetPasswordAction with not exists username inputs
     */
    public function testResetPasswordActionWithNotExistsUsernameInputs()
    {
        $url = $this->container->get('router')->generate('ats_user_user_reset_password');

        $userData = [
            'username' => 'username',
        ];

        $this->client->request('POST', $url, [], [], [], json_encode($userData));
        $response = $this->client->getResponse();

        $this->assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());
        $this->assertInstanceOf(JsonResponse::class, $response);

        $expected = [
            'response' => 'reset_password.username.not_exist',
        ];
        $this->assertEquals($expected, json_decode($response->getContent(), true));
    }

    /**
     * Test suits for resetPasswordAction
     */
    public function testResetPasswordAction()
    {
        $url = $this->container->get('router')->generate('ats_user_user_reset_password');

        $this->userManager->create(
            'username',
            'username@ats-digital.com',
            'password'
        );

        $userData = [
            'username' => 'username',
        ];

        $this->client->request('POST', $url, [], [], [], json_encode($userData));
        $response = $this->client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertInstanceOf(JsonResponse::class, $response);

        $expected = [
            'response' => 'reset_password.success',
        ];
        $this->assertEquals($expected['response'], json_decode($response->getContent(), true)['response']);
    }

    /**
     * Test suits for changePasswordAction with empty inputs
     */
    public function testChangePasswordActionWithEmptyInputs()
    {
        $url = $this->container->get('router')->generate('ats_user_user_change_password');

        $this->client->request('POST', $url);
        $response = $this->client->getResponse();

        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());
        $this->assertInstanceOf(JsonResponse::class, $response);

        $expected = [
            'response' => 'reset_password.missing.token',
        ];
        $this->assertEquals($expected, json_decode($response->getContent(), true));
    }

    /**
     * Test suits for changePasswordAction with empty token
     */
    public function testChangePasswordActionWithEmptyTokenInputs()
    {
        $url = $this->container->get('router')->generate('ats_user_user_change_password');

        $userData = [
            'plain_password' => 'password',
        ];

        $this->client->request('POST', $url, [], [], [], json_encode($userData));
        $response = $this->client->getResponse();

        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());
        $this->assertInstanceOf(JsonResponse::class, $response);

        $expected = [
            'response' => 'reset_password.missing.token',
        ];
        $this->assertEquals($expected['response'], json_decode($response->getContent(), true)['response']);
    }

    /**
     * Test suits for changePasswordAction with empty password
     */
    public function testChangePasswordActionWithEmptyPasswordInputs()
    {
        $url = $this->container->get('router')->generate('ats_user_user_change_password');

        $userData = [
            'reset_token' => 'some_token',
        ];

        $this->client->request('POST', $url, [], [], [], json_encode($userData));
        $response = $this->client->getResponse();

        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());
        $this->assertInstanceOf(JsonResponse::class, $response);

        $expected = [
            'response' => 'reset_password.missing.plain_password',
        ];
        $this->assertEquals($expected['response'], json_decode($response->getContent(), true)['response']);
    }

    /**
     * Test suits for changePasswordAction with invalid token
     */
    public function testChangePasswordActionWithInvalidTokenInputs()
    {
        $url = $this->container->get('router')->generate('ats_user_user_change_password');

        $userData = [
            'plain_password' => "password",
            'reset_token' => 'some_token',
        ];

        $this->client->request('POST', $url, [], [], [], json_encode($userData));
        $response = $this->client->getResponse();

        $this->assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());
        $this->assertInstanceOf(JsonResponse::class, $response);

        $expected = [
            'response' => 'change_password.token.not_exist',
        ];
        $this->assertEquals($expected['response'], json_decode($response->getContent(), true)['response']);
    }
}
