# User Bundle

### General
---
A full featured and restful user bundle based on OAuth2

### Installation
---
1. Update your `composer.json` to add ATS private packagist:
```json
{
    "repositories": [
      // ...
        {
            "type": "composer",
            "url": "https://packagist.ats-digital.com"
        }
        // ...
    ],
    // ...
}
```

2. Install using composer:
```sh
$ php composer require ats/user-bundle dev-master
```

### Configuration
---
1. Bundles registration

To start using this bundle you need to update your `AppKernel.php` file and register the following bundles:

```php
<?php
// app/AppKernel.php

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = [
            // ...
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Doctrine\Bundle\MongoDBBundle\DoctrineMongoDBBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new JMS\SerializerBundle\JMSSerializerBundle(),
            new FOS\OAuthServerBundle\FOSOAuthServerBundle(),
            new ATS\CoreBundle\ATSCoreBundle(),
            new ATS\UserBundle\ATSUserBundle(),
            // ...
        ];
    }
    // ...
}

```
2. Bundle configuration

Default configuration is sufficient, unless you may override it to customize the bundle usage, to do this, update your `config.yml` file and add the following configuration section see [Configuration refrences][config-references]

Configure SwiftMailer
```
swiftmailer:
    transport:  "<transport>" # exp. gmail
    username:   "<username>"
    password:   "<password>"
    spool:
        type: file
        path: '%kernel.project_dir%/var/spool/app/%kernel.environment%'
    antiflood:
        threshold: 99
        sleep:     5
```

3. Security configuration

Update your `security.yml` file and add the following configuration in it, don't hesitate to check the Symfony [documentation][symfony-security] for more informations and details:
```yaml
# app/config/security.yml
security:
    encoders:
        ATS\UserBundle\Document\User: sha512

    role_hierarchy:
        ROLE_ADMIN:       ROLE_USER
        ROLE_SUPER_ADMIN: [ROLE_ADMIN, ROLE_ALLOWED_TO_SWITCH]

    providers:
        ats_user_provider:
            id: ATS\UserBundle\Provider\UserProvider

    firewalls:
        oauth_token:
            pattern:    ^/oauth/v2/token
            security:   false

        main:
            pattern:    ^/ # might be something else
            fos_oauth:  true
            stateless:  true
            anonymous:  false

    # put your own access control rules in here
    # access_control:
        # - { path: ^/, role: ROLE_USER }
```

### Usage
---
1. Create a client:

```sh
$ php bin/console ats:user:client:create <name>
```

The previous command will output something like the following:
```sh
Client Credentials
==================

 +----------+-------------+-----------------+
 | name     | client_id   | client_secret   |
 +----------+-------------+-----------------+
 | <name>   | <client_id> | <client_secret> |
 +----------+-------------+-----------------+

```

2. Create a user:
```sh
$ php bin/console ats:user:create <usenrame> <email> <password>
```

3. Request a OAuth2 Bearer access token:

Send a GET request using the `client_id` and `client_secret` you generated in step 1 with username and password you created in step 2 by setting the `grant_type` as *password*, your request should look like this:

```sh
$ curl -X GET 'http://127.0.0.1:8000/oauth/v2/token?grant_type=password&client_id=<client_id>&client_secret=<client_secret>&redirect_uri=<redirect_uri>&username=<username>&password=<password>'

```

This request will return a reponse in json format with the `access_token`:
```json
{
    "access_token": <access_token>,
    "expires_in": 3600,
    "token_type": "bearer",
    "scope": <scopes>,
    "refresh_token": <refresh_token>
}
```

4. Login using the `access_token`

To login to your application using the previous generated `access_token` you just have to add it in the `header` of your request in the `Authorization` header key and add the keyword `Bearer ` before it:

```sh
$ curl -H 'Authorization: Bearer <access_token>' http://127.0.0.1:8000
```

5. Refresh a OAuth2 Bearer access token:

When your access token is expired you can refresh it by sending a GET request using the `client_id` and `client_secret` you generated in step 1 with username and password you created in step 2 and the `refresh_token` provided in step 3 by setting the `grant_type` as *refresh_token*, your request should look like this:

```sh
$ curl -X GET 'http://127.0.0.1:8000/oauth/v2/token?grant_type=password&client_id=<client_id>&client_secret=<client_secret>&redirect_uri=<redirect_uri>&username=<username>&password=<password>&refresh_token=<refresh_token>'

```

This request will return a reponse in json format with the new `access_token`:
```json
{
    "access_token": <access_token>,
    "expires_in": 3600,
    "token_type": "bearer",
    "scope": <scopes>,
    "refresh_token": <refresh_token>
}
```

That's it.

##### Example

1. Create a client:
```sh
$ php bin/console ats:user:client:create webapp
```
output:
```sh
Created client
+--------+-----------------------------------------------------------------------------+----------------------------------------------------+
| name   | client_id                                                                   | client_secret                                      |
+--------+-----------------------------------------------------------------------------+----------------------------------------------------+
| webapp | 5c0bad1027ff86203709f9a1_3gngcksw79escc0k0c0g4gc00k8kscwo4wks08kc8sk8w4gco4 | 1n8ahgynngxwcs8g8gs8cgg08o8gogk0k8sgogco0cocc8ck4w |
+--------+-----------------------------------------------------------------------------+----------------------------------------------------+

```

2. Create a user:
```sh
$ php bin/console ats:user:create myusername myemail@ats-digital.com mypassword
```

3. Request a OAuth2 Bearer access token:
```sh
$ curl -X GET 'http://127.0.0.1:8000/oauth/v2/token?grant_type=password&client_id=5c0bad1027ff86203709f9a1_3gngcksw79escc0k0c0g4gc00k8kscwo4wks08kc8sk8w4gco4&client_secret=1n8ahgynngxwcs8g8gs8cgg08o8gogk0k8sgogco0cocc8ck4w&redirect_uri=http://127.0.0.1&username=myusername&password=mypassword'
```
response:
```json
{
    "access_token": "N2Y5NTc1ZThiNjgyYWU3NTE1OGZjNTZlYWVhODJkYmQ5NmEzM2I4NzA1YTRmYzU4MGU2MWI3ZGZkNzUwMmI3Yg",
    "expires_in": 3600,
    "token_type": "bearer",
    "scope": "user admin super_admin",
    "refresh_token": "MWRkNjdkNDYwNjBlNjVkMjVmNTMzNGI1Mjc4YWUzMzg3YTY4MTQ5MDFlN2EwMGZmZThjYmI3YzFmMzkzYzQ5ZA"
}
```

4. Login using the `access_token`
```sh
$ curl -H 'Authorization: Bearer N2Y5NTc1ZThiNjgyYWU3NTE1OGZjNTZlYWVhODJkYmQ5NmEzM2I4NzA1YTRmYzU4MGU2MWI3ZGZkNzUwMmI3Yg' http://127.0.0.1:8000
```

3. Refresh a OAuth2 Bearer access token:
```sh
$ curl -X GET 'http://127.0.0.1:8000/oauth/v2/token?grant_type=refresh_token&client_id=5c0bad1027ff86203709f9a1_3gngcksw79escc0k0c0g4gc00k8kscwo4wks08kc8sk8w4gco4&client_secret=1n8ahgynngxwcs8g8gs8cgg08o8gogk0k8sgogco0cocc8ck4w&redirect_uri=http://127.0.0.1&username=myusername&password=mypassword&refresh_token=MWRkNjdkNDYwNjBlNjVkMjVmNTMzNGI1Mjc4YWUzMzg3YTY4MTQ5MDFlN2EwMGZmZThjYmI3YzFmMzkzYzQ5ZA'
```
response:
```json
{
    "access_token": "YTFmNDBlYmZkNDdhNGM1NTNkODY0ODNkNDQ4MmM4YWRmMGQ0ZDM3MDViNjUzMTNlNmYyYzc0MWI1NGQ4NTMyOQ",
    "expires_in": 3600,
    "token_type": "bearer",
    "scope": "user admin super_admin",
    "refresh_token": "ZThlNDI3NjViNzY0NmQyNDM5MzYzYzNiOGMzYmRjYjcxNGU4MDQzZjUwZGE1YTUzZWRmOTFhMTI1YWE5Yzg3OQ"
}
```

### Tests
---
To run unit test:
```sh
$ php ./vendor/bin/simple-phpunit --coverage-text --colors=never --strict-coverage --disallow-test-output -c phpunit.xml.dist
```
### Table of contents
---
[Configuration references][config-references]

---
Enjoy!

[symfony-security]: <https://symfony.com/doc/3.4/security.html>
[config-references]: <https://gitlab.ats-digital.com/ats/user-bundle/blob/master/doc/ConfigurationReferences.md>
