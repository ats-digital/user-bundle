# User Bundle

### Custom user class

If you want to use your own user class to add specific fields update this key with the fully qualified class name.
Unless you are using the `ATSGeneratorBundle` to create your class and want to implement it manually, your class *must* extends from `ATS\UserBundle\Document\User` then your service class, manager class and repository class *must* also extends respectively from `ATS\UserBundle\Service\UserService`, `ATS\UserBundle\Manager\UserManager` and `ATS\UserBundle\Repository\UserRepository`

1. Create User class

```php
<?php
// src/Acme/AppBundle/Document/User.php

namespace Acme\AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use ATS\UserBundle\Docuement\User as BaseUser;

/**
 * User
 *
 * @ODM\Document(repositoryClass="Acme\AppBundle\Repository\UserRepository")
 * @ODM\HasLifecycleCallbacks
 */
class User extends BaseUser
{
}
```

2. Update configuration
```yaml
# app/config/config.yml
ats_user:
    #...
    user_class: Acme\AppBundle\Document\User
```

3. Update security configuration
```yaml
# app/config/security.yml
security:
    #...
    encoders:
        Acme\AppBundle\Document\User: sha512
```

4. Create Service class

```php
<?php
// src/Acme/AppBundle/Service/UserService.php

namespace Acme\AppBundle\Service;

use ATS\UserBundle\Service\UserService as BaseUserService;

class UserService extends BaseUserService
{
}
```

5. Create Manager class

```php
<?php
// src/Acme/AppBundle/Manager/UserManager.php

namespace Acme\AppBundle\Manager;

use ATS\UserBundle\Manager\UserManager as BaseUserManager;

class UserManager extends BaseUserManager
{
    /**
     * Constructor
     *
     * @param ManagerRegistry $managerRegistry
     * @param string          $userClass
     * @param array           $rolesDefinition
     * @param string          $managerName
     */
    public function __construct(ManagerRegistry $managerRegistry, $userClass, array $rolesDefinition, $managerName = null)
    {
        parent::__construct($managerRegistry, $userClass, $rolesDefinition, $managerName);
    }
}
```

6. Register UserManager service

```yaml
# src/Acme/AppBundle/Resources/config/service.yml
services:
    #...
    Acme\AppBundle\Manager\UserManager:
        arguments:
            - "@doctrine_mongodb"
            - "%ats_user.user_class%"
            - "%ats_user.roles_definition%"
            - ~
```

7. Create Repository class

```php
<?php
// src/Acme/AppBundle/Repository/UserRepository.php

namespace Acme\AppBundle\Repository;

use ATS\UserBundle\Repository\UserRepository as BaseUserRepository;

class UserRepository extends BaseUserRepository
{
}
```

Enjoy !
