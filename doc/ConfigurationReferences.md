# User Bundle

### Configuration references
---
```yaml
# app/config/config.yml
ats_user:
    password_strength:    weak # One of "weak"; "low"; "medium"; "strong"
    user_class:           ATS\UserBundle\Document\User
    user_provider:        ATS\UserBundle\Provider\UserProvider
    scopes:               ~
    roles_definition:
        # Defaults:
        - ROLE_USER
        - ROLE_ADMIN
        - ROLE_SUPER_ADMIN
    ip_whitelist:         []
    ip_blacklist:         []
    routing:
        host:                 127.0.0.1
        prefix:               user
        scheme:               http
    routes:
        oauth_token:          /token
        register:             /register
        reset_password:       /reset-password
        change_password:      /change-password
    template:
        reset_password:       'ATSUserBundle:Email:reset-password.html.twig'
    reset_password:
        callback_uri:         'http://127.0.0.1/user/new-password'
        token_parameter:      reset-token
        subject:              'Password reset'
        sender_name:          ATS
        sender_email:         no-reply@ats-digital.com
        save:                 true

```

#### password_strengh:
Define the minimal password strength:

|  Index   |   Description                                                    |
|----------|------------------------------------------------------------------|
| *weak*   |   Minimum: 6 characters                                          |
| *low*    |   Minimum: 6 characters and 1 capital letter                     |
| *medium* |   Minimum: 6 characters, 1 capital letter and 1 number           |
| *string* |   Minimum: 6 characters, 1 capital letter, 1 number and 1 symbol |

#### user_class:
If you want to use your own user class to add specific fields update this key with the fully qualified class name, your class *must* extends from `ATS\UserBundle\Document\Class`, see [create custom user class][custom-user-class]

#### user_provider:
If you want to use your own user provider

1. Create your User provider class, see [create custom user provider][symfony-user-provider]

2. Update configuration
```yaml
# app/config/config.yml
ats_user:
    #...
    user_provider: Acme\AppBundle\Provider\UserProvider
```

3. Update security configuration
```yaml
# app/config/security.yml
security:
    #...
    providers:
        user_provider:
            id: Acme\AppBundle\Provider\UserProvider
```

#### scopes:
The default behavior is to use scopes as [roles][symfony-roles].

1. Update configuration
```yaml
# app/config/config.yml
ats_user:
    scopes: scope1 scope2
```
In the previous example, it would allow us to use the roles ROLE_SCOPE1, and ROLE_SCOPE2 (scopes are automatically uppercased).
> Note: By updating the scope it necessitate to update the `roles_hierarchy` in your security configuration and the `roles_definition` configuration key. Based on the previous example you may do something like the following:

```yaml
# app/config/config.yml
ats_user:
    roles_definition:
        - ROLE_SCOPE1
        - ROLE_SCOPE2
```

```yaml
# app/config/security.yml
security:
    #...
    role_hierarchy:
        ROLE_SCOPE1: ROLE_USER
        ROLE_SCOPE2: [ROLE_SCOPE1, ROLE_ALLOWED_TO_SWITCH]
```

Enjoy !

[custom-user-class]: <https://gitlab.ats-digital.com/ats/user-bundle/blob/master/doc/CustomUserClass.md>
[symfony-user-provider]: <https://symfony.com/doc/3.4/security/custom_provider.html#create-a-user-provider>
[symfony-roles]: <https://symfony.com/doc/3.4/security.html#roles>
